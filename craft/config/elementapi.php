<?php 

namespace Craft;


return [
	'endpoints' => [
		'api/scores.json' => function(){
			return [
				'elementType' => 'Entry',
				'criteria' => [
					'section' => 'spieler',
					'order' => 'score desc'
				],
				'transformer' => function(EntryModel $entry) {

					return [
						'id' => $entry->id,
						'name' => $entry->spielername,
						'score' => $entry->score
					];
				},
				'paginate' => false
			];
		},
	]
];