<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */
return array(
    '*' => array(
		'defaultImageQuality' => 75,
        'omitScriptNameInUrls' => true,
        'limitAutoSlugsToAscii' => true,
        'convertFilenamesToAscii' => true,
        'useEmailAsUsername' => true,
        'defaultSearchTermOptions' => array(
            'subLeft' => true,
            'subRight' => true,
        )
    ),
    'LOCAL' => array(
    	'defaultImageQuality' => 78,
        'devMode' => true,
        'siteUrl' => 'http://bussimulator.craft.dev/',
		'environmentVariables' => array(
            'basePath' => '/Users/andreasbecker/Documents/KOPFMUNTER/web/bussimulator/',
            'uploadPath' => 'http://bussimulator.craft.dev/uploads/',
		)
    ),
    'STAGING' => array(
        'defaultImageQuality' => 80,
        'siteUrl' => 'http://bussimulator.kopfmunter.de/',
        'environmentVariables' => array(
            'basePath' => '/kunden/382426_66802/webseiten/kopfmunter/kunden/bussimulator/',
            'uploadPath' => 'http://bussimulator.kopfmunter.de/uploads/',
        )
    ),
    'PRODUCTION' => array(
    	'defaultImageQuality' => 98,
        'siteUrl' => 'http://bussimulator.de/',
		'environmentVariables' => array(
            'basePath' => '',
            'uploadPath' => 'http://bussimulator.de/uploads/',
		)
    )
);﻿

?>
