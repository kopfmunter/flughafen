<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
    '*' => array(

    ),
    'LOCAL' => array(
		'server' => 'localhost',
		'user' => 'root',
		'password' => 'root',
		'database' => 'bussimulator',
		'tablePrefix' => 'craft',
    ),
    'STAGING' => array(
		'server' => '',
		'user' => '',
		'password' => '',
		'database' => '',
		'tablePrefix' => 'craft',
    ),
    'PRODUCTION' => array(
		'server' => '',
		'user' => '',
		'password' => '',
		'database' => '',
		'tablePrefix' => 'craft',
    )
);﻿

?>