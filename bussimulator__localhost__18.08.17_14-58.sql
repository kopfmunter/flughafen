# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.5.38)
# Datenbank: bussimulator
# Erstellt am: 2017-08-18 12:58:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Export von Tabelle craft_assetfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetfiles`;

CREATE TABLE `craft_assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `craft_assetfiles_sourceId_fk` (`sourceId`),
  KEY `craft_assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `craft_assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_assetfolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetfolders`;

CREATE TABLE `craft_assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `craft_assetfolders_parentId_fk` (`parentId`),
  KEY `craft_assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `craft_assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetindexdata`;

CREATE TABLE `craft_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `craft_assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `craft_assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_assetsources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetsources`;

CREATE TABLE `craft_assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assetsources_handle_unq_idx` (`handle`),
  KEY `craft_assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransformindex`;

CREATE TABLE `craft_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransforms`;

CREATE TABLE `craft_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categories`;

CREATE TABLE `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_categories_groupId_fk` (`groupId`),
  CONSTRAINT `craft_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups`;

CREATE TABLE `craft_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_categorygroups_handle_unq_idx` (`handle`),
  KEY `craft_categorygroups_structureId_fk` (`structureId`),
  KEY `craft_categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_categorygroups_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups_i18n`;

CREATE TABLE `craft_categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `craft_categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_content`;

CREATE TABLE `craft_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_score` int(10) unsigned DEFAULT '0',
  `field_spielername` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_content_title_idx` (`title`),
  KEY `craft_content_locale_fk` (`locale`),
  CONSTRAINT `craft_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_content` WRITE;
/*!40000 ALTER TABLE `craft_content` DISABLE KEYS */;

INSERT INTO `craft_content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_score`, `field_spielername`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'de',NULL,NULL,0,NULL,'2017-06-24 14:06:41','2017-06-25 06:46:53','36ca2448-e9b3-4846-8bbf-f7804ccc6c41'),
	(2,2,'de','Welcome to Bussimulator.craft.dev!','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Bussimulator.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>',0,NULL,'2017-06-24 14:06:46','2017-06-24 14:06:46','e0f19122-a833-41e5-992c-827b06612695'),
	(15,15,'de','Fabian',NULL,49,'Fabian','2017-06-25 09:34:09','2017-06-25 09:34:09','5fbe244a-4929-4d23-a2b8-8e9b98601338'),
	(16,16,'de','Juliette',NULL,18,'Juliette','2017-06-25 09:34:43','2017-06-25 09:34:43','d1700aab-5b3d-4d0d-9104-10fa80704576'),
	(17,17,'de','Joel',NULL,25,'Joel','2017-06-25 09:34:53','2017-06-25 09:34:53','2ffd4813-7695-4574-b93f-fc1fa13ad2e7'),
	(19,19,'de','Tobias',NULL,12,'Tobias','2017-06-25 09:36:47','2017-06-25 09:36:47','58bbf0c4-18f3-4dbc-841e-09f9ed97bd6a'),
	(21,21,'de','Mustermann',NULL,39,'Mustermann','2017-06-25 09:41:34','2017-06-25 09:41:34','175f4b28-9334-4596-b067-455addcac833'),
	(22,22,'de','Mustermann',NULL,25,'Mustermann','2017-06-25 09:41:43','2017-06-25 09:41:43','ce374d1c-7d74-4e40-ab59-3363865b6368'),
	(23,23,'de','Musterfrau',NULL,11,'Musterfrau','2017-06-25 09:41:51','2017-06-25 09:41:51','79f2e03b-b8a4-4c72-8df0-c8f3aa80a7aa'),
	(24,24,'de','Marvin',NULL,44,'Marvin','2017-06-25 09:42:39','2017-06-25 09:42:39','7cf8a780-729d-43df-aa26-37105800d919'),
	(25,25,'de','Oliver',NULL,36,'Oliver','2017-06-25 09:46:59','2017-06-25 09:46:59','d688dd76-4e91-4239-a126-dbd9bd4ca46d'),
	(26,26,'de','Emely',NULL,5,'Emely','2017-06-25 09:52:49','2017-06-25 09:52:49','6b504261-6514-47b1-97f4-6aa35925701a'),
	(27,27,'de','Emely',NULL,5,'Emely','2017-06-25 09:52:56','2017-06-25 09:52:56','e400e229-5b9b-4862-9713-4216cfab8bee'),
	(28,28,'de','Marie',NULL,13,'Marie','2017-06-25 09:56:13','2017-06-25 09:56:13','b27483f0-171c-44bc-8f57-157890aacf6b'),
	(29,29,'de','Tim',NULL,62,'Tim','2017-06-25 10:03:08','2017-06-25 10:03:08','380f8a6e-1b5e-4759-9f25-23421e4d4949'),
	(30,30,'de','Dennis',NULL,69,'Dennis','2017-06-25 10:06:16','2017-06-25 10:06:16','d8535008-90fa-46f1-b41d-60d0d2864893'),
	(31,31,'de','Vlad',NULL,45,'Vlad','2017-06-25 10:09:27','2017-06-25 10:09:27','fc8f7f3f-51ad-4d78-8b94-c87b8a1e1e05'),
	(32,32,'de','Patrick',NULL,41,'Patrick','2017-06-25 10:18:52','2017-06-25 10:18:52','8557425a-87b4-456d-8f76-58165c2fbaf9'),
	(33,33,'de','Marco',NULL,38,'Marco','2017-06-25 10:24:15','2017-06-25 10:24:15','c313633c-b742-42b4-b233-d8f1f5bb7333'),
	(34,34,'de','Detlef',NULL,47,'Detlef','2017-06-25 10:34:07','2017-06-25 10:34:07','a141f3a4-70b7-4eea-9b8b-90e5093be611'),
	(35,35,'de','Yannik',NULL,18,'Yannik','2017-06-25 10:42:16','2017-06-25 10:42:16','08260752-45d7-403f-b8a4-23e1844d4d83'),
	(36,36,'de','Max',NULL,42,'Max','2017-06-25 10:44:59','2017-06-25 10:44:59','a5e752cc-9445-4048-a334-ac37c3d1c1ab'),
	(37,37,'de','Felix',NULL,49,'Felix','2017-06-25 10:47:36','2017-06-25 10:47:36','3e0f029a-c1c5-49fa-a587-0d4dddc6bd3d'),
	(38,38,'de','Daniel',NULL,73,'Daniel','2017-06-25 10:49:56','2017-06-25 10:49:56','602ccc2d-cf45-4a23-8aff-083b0a448cdd'),
	(39,39,'de','Alex',NULL,73,'Alex','2017-06-25 10:52:33','2017-06-25 10:52:33','191d57f8-4203-4cda-8379-2181de4035e9'),
	(40,40,'de','Silas',NULL,18,'Silas','2017-06-25 10:55:40','2017-06-25 10:55:40','9e3e2cb6-af53-4043-8829-cf462770ead5'),
	(41,41,'de','Ole',NULL,32,'Ole','2017-06-25 10:58:24','2017-06-25 10:58:24','9f3eb2d2-efee-4116-ab3d-c446ad0a3a96'),
	(42,42,'de','Bian',NULL,36,'Bian','2017-06-25 11:01:22','2017-06-25 11:01:22','964aa43e-b942-4014-a46a-dd8931175089'),
	(43,43,'de','Elian',NULL,68,'Elian','2017-06-25 11:04:01','2017-06-25 11:04:01','7a357c54-8918-41fd-b465-f4b1913bac66'),
	(44,44,'de','Tanja',NULL,13,'Tanja','2017-06-25 11:06:44','2017-06-25 11:06:44','60934fa1-b298-429a-943b-a6d3b788b5d9'),
	(45,45,'de','Adrian',NULL,73,'Adrian','2017-06-25 11:09:33','2017-06-25 11:09:33','b02d57a3-50c9-48ed-ad84-c912f1bf55c8'),
	(46,46,'de','Meliass',NULL,0,'Meliass','2017-06-25 11:12:16','2017-06-25 11:12:16','610798d5-cf84-4d45-8830-0aac179e7402'),
	(47,47,'de','Melissa',NULL,36,'Melissa','2017-06-25 11:12:29','2017-06-25 11:12:29','eaffcc01-2ef4-41df-946f-9b08ef9a4715'),
	(48,48,'de','Benedict',NULL,54,'Benedict','2017-06-25 11:14:54','2017-06-25 11:14:54','4a298cc5-bd93-4779-b098-0a9bfb4bdc6f'),
	(49,49,'de','Mara',NULL,45,'Mara','2017-06-25 11:20:18','2017-06-25 11:20:18','31acbab2-26d8-452b-8d11-e17243aaf79a'),
	(50,50,'de','Robin',NULL,56,'Robin','2017-06-25 11:23:07','2017-06-25 11:23:07','2b20e31f-fd35-45fc-9e6a-da6bbfc9deee'),
	(51,51,'de','Michael',NULL,36,'Michael','2017-06-25 11:26:17','2017-06-25 11:26:17','380be8f8-6cf3-4c08-8395-8f1e347c5394'),
	(52,52,'de','Alexander',NULL,42,'Alexander','2017-06-25 11:28:57','2017-06-25 11:28:57','44fb6fcb-63d0-4317-b90b-fdc93c56384f'),
	(53,53,'de','Waldemar',NULL,11,'Waldemar','2017-06-25 11:34:23','2017-06-25 11:34:23','593f04a8-1e4e-4e81-8c1a-21bd2523b3af'),
	(54,54,'de','Philipp',NULL,38,'Philipp','2017-06-25 11:34:41','2017-06-25 11:34:41','3c5d828f-c946-4b6a-8231-33fa48cbf8d6'),
	(55,55,'de','Jenni',NULL,62,'Jenni','2017-06-25 11:37:31','2017-06-25 11:37:31','4362d2b9-158c-4949-bb58-aca35bf031bf'),
	(56,56,'de','Max',NULL,24,'Max','2017-06-25 11:39:59','2017-06-25 11:39:59','6e66fcfb-bc72-468c-a001-86dfa4e2b31b'),
	(57,57,'de','Jasmin',NULL,54,'Jasmin','2017-06-25 11:42:36','2017-06-25 11:42:36','c5ff63d4-c7b0-40bd-ab7c-becfe9c785d3'),
	(58,58,'de','Luis',NULL,34,'Luis','2017-06-25 11:45:17','2017-06-25 11:45:17','90168acf-3396-45d3-a6c5-29cb067b9c66'),
	(59,59,'de','Nils',NULL,49,'Nils','2017-06-25 11:47:49','2017-06-25 11:47:49','1b96aabf-9460-4693-80ee-006185a23e0d'),
	(60,60,'de','Ole',NULL,97,'Ole','2017-06-25 11:50:16','2017-06-25 11:50:16','b1fb3cf3-9944-4fca-b094-7ec3389f9f9e'),
	(61,61,'de','Fabian',NULL,49,'Fabian','2017-06-25 11:52:47','2017-06-25 11:52:47','d0b96520-cb5a-45bf-b0a5-d81fc9c274cd'),
	(62,62,'de','Justin',NULL,13,'Justin','2017-06-25 11:55:28','2017-06-25 11:55:28','84fbd86a-5786-430f-9ffd-cc0f79477315'),
	(63,63,'de','Larissa',NULL,5,'Larissa','2017-06-25 11:57:50','2017-06-25 11:57:50','75d47b0d-383e-4959-b688-d9b9b09f453e'),
	(64,64,'de','Lenden',NULL,73,'Lenden','2017-06-25 12:00:29','2017-06-25 12:00:29','3d7c3eca-18fc-4588-95aa-a4035d8c08aa'),
	(65,65,'de','Leonard',NULL,38,'Leonard','2017-06-25 12:03:12','2017-06-25 12:03:12','48c98773-5663-405d-940a-b3d581723bec'),
	(66,66,'de','Fin',NULL,13,'Fin','2017-06-25 12:05:46','2017-06-25 12:05:46','263c0294-88c8-4aee-a709-7e45173a0f53'),
	(67,67,'de','Jan',NULL,7,'Jan','2017-06-25 12:08:21','2017-06-25 12:08:21','68e5646a-547b-4ef4-9310-a5f2c22ad64e'),
	(68,68,'de','Jan',NULL,44,'Jan','2017-06-25 12:10:20','2017-06-25 12:10:20','7ff9cb1a-1954-4ab0-a3f0-e008b48fe345'),
	(69,69,'de','Michael',NULL,12,'Michael','2017-06-25 12:13:00','2017-06-25 12:13:00','fb2b778e-14d1-4006-90f3-0e53220dd7de'),
	(70,70,'de','Merle',NULL,34,'Merle','2017-06-25 12:15:33','2017-06-25 12:15:33','4e8314bb-7556-4cd5-8275-c3b1ddcaf510'),
	(71,71,'de','Selma',NULL,13,'Selma','2017-06-25 12:18:20','2017-06-25 12:18:20','77886fdb-258c-431e-970c-73e7ce6459bb'),
	(72,72,'de','Luana',NULL,13,'Luana','2017-06-25 12:21:01','2017-06-25 12:21:01','22dd0860-bf66-4d62-9664-fd228cf3a4e8'),
	(73,73,'de','Emmely',NULL,56,'Emmely','2017-06-25 12:23:43','2017-06-25 12:23:43','f85410e7-c17e-4de6-887c-ef8b17821335'),
	(74,74,'de','Dalleen',NULL,49,'Dalleen','2017-06-25 12:26:21','2017-06-25 12:26:21','8452da75-3748-467c-b4f3-78eeccdd94b3'),
	(75,75,'de','Daniel',NULL,49,'Daniel','2017-06-25 12:29:15','2017-06-25 12:29:15','c15d0127-94c7-4394-bc63-c40bcbc46cea'),
	(76,76,'de','Ben',NULL,32,'Ben','2017-06-25 12:32:31','2017-06-25 12:32:31','216a08aa-0851-40ca-83a4-d327f46eff48'),
	(77,77,'de','Marcel',NULL,49,'Marcel','2017-06-25 12:35:20','2017-06-25 12:35:20','e62066dd-a344-4f8b-8c87-5ad915286f78'),
	(78,78,'de','Jamel',NULL,18,'Jamel','2017-06-25 12:38:23','2017-06-25 12:38:23','ab000672-d3b2-46d6-a049-c0621d720946'),
	(79,79,'de','Emrulah',NULL,40,'Emrulah','2017-06-25 12:41:35','2017-06-25 12:41:35','d08bd2d4-db27-4f17-8fc5-635e81dc2215'),
	(80,80,'de','Matthias',NULL,13,'Matthias','2017-06-25 12:44:42','2017-06-25 12:44:42','c59ac9cb-a48b-4f42-b75a-9e9e50d64f92'),
	(81,81,'de','David',NULL,11,'David','2017-06-25 12:47:31','2017-06-25 12:47:31','71cb6583-4731-4171-911c-c1fecb9e3337'),
	(82,82,'de','Daniele',NULL,13,'Daniele','2017-06-25 12:50:14','2017-06-25 12:50:14','01b720d3-b313-43cf-97c9-acfe5e29db07'),
	(83,83,'de','Landon',NULL,44,'Landon','2017-06-25 12:53:03','2017-06-25 12:53:03','634db882-da36-4932-aa15-535c369e8416'),
	(84,84,'de','Michael',NULL,33,'Michael','2017-06-25 12:56:17','2017-06-25 12:56:17','8448a14d-3843-4da2-a837-2944095c9712'),
	(85,85,'de','Jens',NULL,0,'Jens','2017-06-25 12:58:09','2017-06-25 12:58:09','1a6ae187-9ee5-4883-b2a1-89d72801db4d'),
	(86,86,'de','Marie',NULL,38,'Marie','2017-06-25 13:00:40','2017-06-25 13:00:40','d0d76d62-5af7-46eb-8bdb-7d1cfd9d087d'),
	(87,87,'de','Thomas',NULL,69,'Thomas','2017-06-25 13:03:37','2017-06-25 13:03:37','9e06f68d-1e49-4fec-9e00-1fae4d9dd681'),
	(88,88,'de','Hendrik',NULL,52,'Hendrik','2017-06-25 13:06:22','2017-06-25 13:06:22','0b187e85-aa42-470c-becd-e95aa1bee90f'),
	(89,89,'de','Alfred',NULL,12,'Alfred','2017-06-25 13:09:05','2017-06-25 13:09:05','66caeea8-ee27-430f-a89b-0fb689308af7'),
	(90,90,'de','Michelle',NULL,36,'Michelle','2017-06-25 13:11:41','2017-06-25 13:11:41','4de44fe3-8341-426e-a61a-abe585db9f2c'),
	(91,91,'de','Max',NULL,69,'Max','2017-06-25 13:14:12','2017-06-25 13:14:12','5f5c112f-5d79-4433-acc9-8022e9aff67c'),
	(92,92,'de','Rojin',NULL,23,'Rojin','2017-06-25 13:17:07','2017-06-25 13:17:07','0af0956b-08f0-4ca6-a768-9fda935ffd58'),
	(93,93,'de','Laurenz',NULL,41,'Laurenz','2017-06-25 13:19:48','2017-06-25 13:19:48','90dc36ea-ea2a-4799-b8de-42c92162df81'),
	(94,94,'de','Emrullah',NULL,40,'Emrullah','2017-06-25 13:22:27','2017-06-25 13:22:27','0698ff66-676b-4532-9454-a0b70f6c6bf1'),
	(95,95,'de','Marie',NULL,42,'Marie','2017-06-25 13:25:12','2017-06-25 13:25:12','f6389024-9c0b-40c5-ab89-bf1253b62ea1'),
	(96,96,'de','Lena',NULL,54,'Lena','2017-06-25 13:27:46','2017-06-25 13:27:46','69127cb8-83b8-4163-bd74-8e44b4262284'),
	(97,97,'de','Zeynab',NULL,49,'Zeynab','2017-06-25 13:30:45','2017-06-25 13:30:45','ed581ba0-cdee-4664-97b6-f960431237b2'),
	(98,98,'de','Anton',NULL,20,'Anton','2017-06-25 13:38:12','2017-06-25 13:38:12','4cf7cc30-a768-43cc-aeff-5beb54a8facd'),
	(99,99,'de','Timo',NULL,15,'Timo','2017-06-25 13:40:52','2017-06-25 13:40:52','a0b9ec84-29a9-450d-9fa8-4b5c55686b87'),
	(100,100,'de','Vinh',NULL,42,'Vinh','2017-06-25 13:43:34','2017-06-25 13:43:34','750bb454-33b5-4c88-a79f-abf11cc7f330'),
	(101,101,'de','Joshua',NULL,57,'Joshua','2017-06-25 13:46:12','2017-06-25 13:46:12','5308c286-be0e-48a6-923e-b7745c014dc0'),
	(102,102,'de','Lukas',NULL,49,'Lukas','2017-06-25 13:48:46','2017-06-25 13:48:46','375fcbfc-6a94-4a8c-acaa-1b27cbb19d4e'),
	(103,103,'de','Lukas',NULL,49,'Lukas','2017-06-25 13:48:47','2017-06-25 13:48:47','37eba678-37fb-4710-800d-b2391493e747'),
	(104,104,'de','Justin',NULL,60,'Justin','2017-06-25 13:51:35','2017-06-25 13:51:35','bbf318c9-4db3-4f2b-9ce6-ada8404581b2'),
	(105,105,'de','Peter',NULL,36,'Peter','2017-06-25 13:54:13','2017-06-25 13:54:13','92865c88-ec8c-4a95-9fdf-25ac53089646'),
	(106,106,'de','Emily',NULL,56,'Emily','2017-06-25 13:56:46','2017-06-25 13:56:46','d3c12944-5972-49ec-82c1-e21a2eb5397d'),
	(107,107,'de','Darleen',NULL,52,'Darleen','2017-06-25 13:59:13','2017-06-25 13:59:13','a8976d67-0e8a-4b29-be77-6e63b8d36159'),
	(108,108,'de','Alex',NULL,36,'Alex','2017-06-25 14:01:45','2017-06-25 14:01:45','f46f4828-79db-47f6-beff-52d46d4a0aeb'),
	(109,109,'de','Tailan',NULL,11,'Tailan','2017-06-25 14:04:26','2017-06-25 14:04:26','6ddc5709-3257-484a-96d6-20b966634d31'),
	(110,110,'de','Kevin',NULL,49,'Kevin','2017-06-25 14:09:27','2017-06-25 14:09:27','7fd49807-8570-4bbf-b295-bd43f81cbead'),
	(111,111,'de','Kevin',NULL,36,'Kevin','2017-06-25 14:12:00','2017-06-25 14:12:00','2bd0ff1a-b529-424d-b06b-1b29730dcbd1'),
	(112,112,'de','Leonas',NULL,20,'Leonas','2017-06-25 14:14:36','2017-06-25 14:14:36','f2a7d190-2871-416a-81ad-1ed599a1d9cf'),
	(113,113,'de','Jens',NULL,37,'Jens','2017-06-25 14:19:28','2017-06-25 14:19:28','0172c3fe-b72a-4fb8-bb00-cab0c72d568c'),
	(114,114,'de','Michael',NULL,45,'Michael','2017-06-25 14:22:08','2017-06-25 14:22:08','6802981e-5c3e-4f22-af9a-d1a5aea00062'),
	(115,115,'de','Marie',NULL,34,'Marie','2017-06-25 14:24:37','2017-06-25 14:24:37','de676dfd-ade0-4287-9373-4e65c49aad40'),
	(116,116,'de','Oliver',NULL,34,'Oliver','2017-06-25 14:29:48','2017-06-25 14:29:48','555c2042-1870-46c3-b4ed-da325142c145'),
	(117,117,'de','Kai',NULL,18,'Kai','2017-06-25 14:32:46','2017-06-25 14:32:46','bafb0b43-b04a-4201-86ef-c6fde7580eef'),
	(118,118,'de','Anna',NULL,12,'Anna','2017-06-25 14:35:24','2017-06-25 14:35:24','7d8a9661-f3aa-475b-98b2-6c4df592f618'),
	(119,119,'de','Katharina',NULL,39,'Katharina','2017-06-25 14:48:04','2017-06-25 14:48:04','672cdb2d-f2a1-4c2e-8ae6-35bec313bc12'),
	(120,120,'de','Leonas',NULL,49,'Leonas','2017-06-25 14:50:40','2017-06-25 14:50:40','c9fa9e2f-034c-4315-a885-cbf2dcb952ef'),
	(121,121,'de','Anette',NULL,43,'Anette','2017-06-25 14:53:47','2017-06-25 14:53:47','a78850ac-9509-4ca7-8cae-368ab519550c'),
	(122,122,'de','Maik',NULL,38,'Maik','2017-06-25 14:56:36','2017-06-25 14:56:36','1ba819b7-26a0-4567-9476-da73a15c93bd'),
	(123,123,'de','Elsa',NULL,36,'Elsa','2017-06-25 15:04:56','2017-06-25 15:04:56','498a5c17-7351-4f67-8ec8-3da64174e795'),
	(124,124,'de','Peter',NULL,49,'Peter','2017-06-25 15:09:59','2017-06-25 15:09:59','64d1d864-8fed-4787-818f-d24266ea4c37'),
	(125,125,'de','Teresa',NULL,6,'Teresa','2017-06-25 15:12:42','2017-06-25 15:12:42','40d9adf7-6f32-44b4-9367-11495662ebe5'),
	(126,126,'de','Susanna',NULL,5,'Susanna','2017-06-25 15:18:59','2017-06-25 15:18:59','a1494307-17ae-456c-a844-6aa8b741a745'),
	(127,127,'de','David',NULL,39,'David','2017-06-25 15:21:42','2017-06-25 15:21:42','43ee640a-05b6-429d-8ac5-a670d58e444c'),
	(128,128,'de','Dustin',NULL,38,'Dustin','2017-06-25 15:26:33','2017-06-25 15:26:33','a88bca5f-14de-4bed-9a47-36506d7e07d1'),
	(129,129,'de','Leon',NULL,56,'Leon','2017-06-25 15:29:13','2017-06-25 15:29:13','0677f601-7ab0-4e13-85d5-a57bc9ae765b'),
	(130,130,'de','Christoph',NULL,53,'Christoph','2017-06-25 15:33:28','2017-06-25 15:33:28','a9c500a3-d67a-4102-a962-74883b4446c7'),
	(131,131,'de','Ralph',NULL,46,'Ralph','2017-06-25 15:36:08','2017-06-25 15:36:08','b399894b-8d3e-4211-8698-b0b6f73f9858');

/*!40000 ALTER TABLE `craft_content` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_deprecationerrors`;

CREATE TABLE `craft_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elementindexsettings`;

CREATE TABLE `craft_elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elementindexsettings` WRITE;
/*!40000 ALTER TABLE `craft_elementindexsettings` DISABLE KEYS */;

INSERT INTO `craft_elementindexsettings` (`id`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Entry','{\"sources\":{\"section:3\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"link\",\"4\":\"field:4\"}}}}','2017-06-25 09:40:10','2017-06-25 09:40:10','740bcfc1-44ff-4e71-8e78-2659486580cc');

/*!40000 ALTER TABLE `craft_elementindexsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements`;

CREATE TABLE `craft_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_elements_type_idx` (`type`),
  KEY `craft_elements_enabled_idx` (`enabled`),
  KEY `craft_elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements` WRITE;
/*!40000 ALTER TABLE `craft_elements` DISABLE KEYS */;

INSERT INTO `craft_elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'User',1,0,'2017-06-24 14:06:41','2017-06-25 06:46:53','ca5a87a5-a8a4-432e-901c-522b446e0670'),
	(2,'Entry',1,0,'2017-06-24 14:06:46','2017-06-24 14:06:46','abde86e3-6523-4786-b05a-02f013911b32'),
	(15,'Entry',1,0,'2017-06-25 09:34:09','2017-06-25 09:34:09','79428ec4-fede-48c3-afc5-9580530f2f7e'),
	(16,'Entry',1,0,'2017-06-25 09:34:43','2017-06-25 09:34:43','19877005-86d4-4c02-ab39-ab63a362d376'),
	(17,'Entry',1,0,'2017-06-25 09:34:53','2017-06-25 09:34:53','cbbdca74-1007-40af-a2c2-c917a35b4acb'),
	(19,'Entry',1,0,'2017-06-25 09:36:47','2017-06-25 09:36:47','866dff5e-6121-4fd6-b45e-ff636bbf0122'),
	(21,'Entry',1,0,'2017-06-25 09:41:34','2017-06-25 09:41:34','09886dee-d254-4e5d-a607-ad0c8849a957'),
	(22,'Entry',1,0,'2017-06-25 09:41:43','2017-06-25 09:41:43','b197f02c-2c74-43b3-ae7e-3ef5e2287ecc'),
	(23,'Entry',1,0,'2017-06-25 09:41:51','2017-06-25 09:41:51','cc7b94d5-8fb4-4822-8225-942cc4e24cdd'),
	(24,'Entry',1,0,'2017-06-25 09:42:39','2017-06-25 09:42:39','12347dd2-e542-4c7c-bddb-a7de9b69cd21'),
	(25,'Entry',1,0,'2017-06-25 09:46:59','2017-06-25 09:46:59','6fa08be2-6b30-4501-a7fe-520ad7e18336'),
	(26,'Entry',1,0,'2017-06-25 09:52:49','2017-06-25 09:52:49','4725b1b6-7b08-4ef8-b190-7115c93ba4f8'),
	(27,'Entry',1,0,'2017-06-25 09:52:56','2017-06-25 09:52:56','d5ee700a-d6ae-4a74-8f48-c1aa95a925bc'),
	(28,'Entry',1,0,'2017-06-25 09:56:13','2017-06-25 09:56:13','5c85cb4d-a920-4320-ad63-76956d533bae'),
	(29,'Entry',1,0,'2017-06-25 10:03:08','2017-06-25 10:03:08','401b7891-b134-48f0-97e2-4a30ac67c18c'),
	(30,'Entry',1,0,'2017-06-25 10:06:16','2017-06-25 10:06:16','b2662048-5b8d-4039-a788-562889a370dd'),
	(31,'Entry',1,0,'2017-06-25 10:09:27','2017-06-25 10:09:27','2f45c07f-903e-4239-bd7d-7bbfa621bba3'),
	(32,'Entry',1,0,'2017-06-25 10:18:52','2017-06-25 10:18:52','b116829b-4be3-4cfc-aec5-5fb34f3ee4c5'),
	(33,'Entry',1,0,'2017-06-25 10:24:15','2017-06-25 10:24:15','cf9d2301-fe2d-4312-8ca6-31a860909beb'),
	(34,'Entry',1,0,'2017-06-25 10:34:07','2017-06-25 10:34:07','2e327f8e-291b-4fa6-825a-8e8eca3e49d0'),
	(35,'Entry',1,0,'2017-06-25 10:42:16','2017-06-25 10:42:16','7818ec60-f135-4df6-96cb-299bda657ca0'),
	(36,'Entry',1,0,'2017-06-25 10:44:59','2017-06-25 10:44:59','4d35cad8-34ee-4a7d-9af8-62c7541ef4bc'),
	(37,'Entry',1,0,'2017-06-25 10:47:36','2017-06-25 10:47:36','caab010e-a760-4b87-bb60-3d89cab0e578'),
	(38,'Entry',1,0,'2017-06-25 10:49:56','2017-06-25 10:49:56','3bb90d86-95d7-4454-8931-1fafa31c9c71'),
	(39,'Entry',1,0,'2017-06-25 10:52:33','2017-06-25 10:52:33','5527fc89-638f-4bf5-bc84-8b7fcc5b79e5'),
	(40,'Entry',1,0,'2017-06-25 10:55:40','2017-06-25 10:55:40','f374a024-4244-4f7e-a612-de08b1c18c9a'),
	(41,'Entry',1,0,'2017-06-25 10:58:24','2017-06-25 10:58:24','33dffc51-9f7d-4d10-9761-05d01844ad95'),
	(42,'Entry',1,0,'2017-06-25 11:01:22','2017-06-25 11:01:22','b2ed43f8-fde9-4167-9d66-4f6db7902406'),
	(43,'Entry',1,0,'2017-06-25 11:04:01','2017-06-25 11:04:01','b93764c4-f8a1-4903-a36b-52b4dbbb3e9b'),
	(44,'Entry',1,0,'2017-06-25 11:06:44','2017-06-25 11:06:44','a8d880fc-5bfb-45ed-b582-45bb45011003'),
	(45,'Entry',1,0,'2017-06-25 11:09:33','2017-06-25 11:09:33','cca9b63d-c7f7-4ff8-bb6d-d7077805248d'),
	(46,'Entry',1,0,'2017-06-25 11:12:16','2017-06-25 11:12:16','5e009790-30fa-4df4-a29d-9ab57fe4af01'),
	(47,'Entry',1,0,'2017-06-25 11:12:29','2017-06-25 11:12:29','dd354dd8-481e-43c1-a5aa-475d848a911d'),
	(48,'Entry',1,0,'2017-06-25 11:14:54','2017-06-25 11:14:54','ea9c1c25-e4ec-4f3f-b561-fefade398270'),
	(49,'Entry',1,0,'2017-06-25 11:20:18','2017-06-25 11:20:18','e84f3c20-c9eb-4d73-8159-184bf65a6cbc'),
	(50,'Entry',1,0,'2017-06-25 11:23:07','2017-06-25 11:23:07','03894019-0ad8-4557-b407-2fc005e8e121'),
	(51,'Entry',1,0,'2017-06-25 11:26:17','2017-06-25 11:26:17','5831ec3b-1a8d-4d32-9c39-787a4199b7b8'),
	(52,'Entry',1,0,'2017-06-25 11:28:57','2017-06-25 11:28:57','477350c0-4153-4159-abb8-c82b10c2f74b'),
	(53,'Entry',1,0,'2017-06-25 11:34:23','2017-06-25 11:34:23','58bc4137-67af-4c93-95ff-a01afca2c3be'),
	(54,'Entry',1,0,'2017-06-25 11:34:41','2017-06-25 11:34:41','86b4ea87-8243-4e3b-88aa-dce86233ca1c'),
	(55,'Entry',1,0,'2017-06-25 11:37:31','2017-06-25 11:37:31','a6359514-b927-4db2-b0b6-3e353445bf71'),
	(56,'Entry',1,0,'2017-06-25 11:39:59','2017-06-25 11:39:59','0cb61ca0-ed60-4eb0-9107-b76a90e6e0d7'),
	(57,'Entry',1,0,'2017-06-25 11:42:36','2017-06-25 11:42:36','fe6b2f6a-9c07-46e7-9317-e0d1962e31c9'),
	(58,'Entry',1,0,'2017-06-25 11:45:17','2017-06-25 11:45:17','97d80ddf-1256-4696-bc3a-1c7a98e80765'),
	(59,'Entry',1,0,'2017-06-25 11:47:49','2017-06-25 11:47:49','6e6a9df6-c62e-4e5d-b4ca-948c20b44367'),
	(60,'Entry',1,0,'2017-06-25 11:50:16','2017-06-25 11:50:16','e47b4a39-6369-4fab-93c4-9973ffdc1278'),
	(61,'Entry',1,0,'2017-06-25 11:52:47','2017-06-25 11:52:47','04afd2ea-effc-40a2-aac5-688e03eb0ae6'),
	(62,'Entry',1,0,'2017-06-25 11:55:28','2017-06-25 11:55:28','3de3387c-a897-4195-b02b-ae4743e86445'),
	(63,'Entry',1,0,'2017-06-25 11:57:49','2017-06-25 11:57:49','a8c30516-f749-4e05-b8e4-c408ba12a9d1'),
	(64,'Entry',1,0,'2017-06-25 12:00:29','2017-06-25 12:00:29','66b58ef5-9461-4233-b7a0-0e5ca57ce396'),
	(65,'Entry',1,0,'2017-06-25 12:03:12','2017-06-25 12:03:12','4293f793-b50b-4c65-92f3-9b08c92dd983'),
	(66,'Entry',1,0,'2017-06-25 12:05:46','2017-06-25 12:05:46','49b0e9cb-22fb-4188-be45-96a488567a56'),
	(67,'Entry',1,0,'2017-06-25 12:08:21','2017-06-25 12:08:21','f9b337f4-9b69-4a81-b008-34fbed598c1a'),
	(68,'Entry',1,0,'2017-06-25 12:10:20','2017-06-25 12:10:20','f51619f5-c66d-470f-8c77-61272062d526'),
	(69,'Entry',1,0,'2017-06-25 12:13:00','2017-06-25 12:13:00','efc3b36b-91b7-42ae-9daa-fbb0223010cc'),
	(70,'Entry',1,0,'2017-06-25 12:15:33','2017-06-25 12:15:33','6779e9e5-0398-4f5c-a80c-734bbb557bfd'),
	(71,'Entry',1,0,'2017-06-25 12:18:20','2017-06-25 12:18:20','a37dddfc-7319-49d6-bc10-035cbd9bfe52'),
	(72,'Entry',1,0,'2017-06-25 12:21:01','2017-06-25 12:21:01','cbc1e9d3-6c29-4763-917c-84999f219c6c'),
	(73,'Entry',1,0,'2017-06-25 12:23:43','2017-06-25 12:23:43','104968ff-d0fc-44a3-90eb-70a8600755b8'),
	(74,'Entry',1,0,'2017-06-25 12:26:21','2017-06-25 12:26:21','ab264b0f-1bd5-4a6f-bf59-d4e44351c46c'),
	(75,'Entry',1,0,'2017-06-25 12:29:15','2017-06-25 12:29:15','df84d251-1a68-4e7f-96f1-19aa6dfe0f9d'),
	(76,'Entry',1,0,'2017-06-25 12:32:31','2017-06-25 12:32:31','8b4cbe31-4800-4948-aa3f-71218cda18c6'),
	(77,'Entry',1,0,'2017-06-25 12:35:20','2017-06-25 12:35:20','9e9c5962-ff4a-4aab-9a8f-494cc69e29d4'),
	(78,'Entry',1,0,'2017-06-25 12:38:23','2017-06-25 12:38:23','46fe4073-59b7-415c-a236-b66b8c1dd691'),
	(79,'Entry',1,0,'2017-06-25 12:41:35','2017-06-25 12:41:35','54d90519-7a67-4770-a245-96284918535e'),
	(80,'Entry',1,0,'2017-06-25 12:44:42','2017-06-25 12:44:42','e6378517-646f-4ff5-af23-545119fd2d5b'),
	(81,'Entry',1,0,'2017-06-25 12:47:31','2017-06-25 12:47:31','73f3c03c-3336-4df6-a976-9edbffa403b8'),
	(82,'Entry',1,0,'2017-06-25 12:50:14','2017-06-25 12:50:14','56cb912e-e490-444d-bb99-21dca7f28000'),
	(83,'Entry',1,0,'2017-06-25 12:53:03','2017-06-25 12:53:03','b84742c6-82bc-446b-945d-2b08d6b38b45'),
	(84,'Entry',1,0,'2017-06-25 12:56:17','2017-06-25 12:56:17','a01569e7-5ab3-4f20-a388-710b661da3ad'),
	(85,'Entry',1,0,'2017-06-25 12:58:09','2017-06-25 12:58:09','d0146f15-de1e-48a0-ae15-d800b3094282'),
	(86,'Entry',1,0,'2017-06-25 13:00:40','2017-06-25 13:00:40','a6f5d3cf-2273-4525-82b0-1993d66a2348'),
	(87,'Entry',1,0,'2017-06-25 13:03:37','2017-06-25 13:03:37','a96316df-6072-484e-8596-0b89ad49a974'),
	(88,'Entry',1,0,'2017-06-25 13:06:22','2017-06-25 13:06:22','e0be169b-4f77-4e44-9bba-b977e1c6b4da'),
	(89,'Entry',1,0,'2017-06-25 13:09:05','2017-06-25 13:09:05','b0dbf833-0903-4f63-b2b7-a998afa1790e'),
	(90,'Entry',1,0,'2017-06-25 13:11:41','2017-06-25 13:11:41','9dd6c49c-2c76-4c5c-aef4-56e645e4c658'),
	(91,'Entry',1,0,'2017-06-25 13:14:12','2017-06-25 13:14:12','ffff0b15-3163-4032-9664-9ee7f15734ff'),
	(92,'Entry',1,0,'2017-06-25 13:17:07','2017-06-25 13:17:07','480aa865-e8d8-4f08-87f1-9da959946080'),
	(93,'Entry',1,0,'2017-06-25 13:19:48','2017-06-25 13:19:48','8385dd05-a9d3-4b2f-9c92-b4680a34f054'),
	(94,'Entry',1,0,'2017-06-25 13:22:27','2017-06-25 13:22:27','22225daa-61c2-45c3-b942-23689e0b6cfc'),
	(95,'Entry',1,0,'2017-06-25 13:25:12','2017-06-25 13:25:12','9f3b2607-4b3a-4668-82de-d81cf7ceece0'),
	(96,'Entry',1,0,'2017-06-25 13:27:46','2017-06-25 13:27:46','d35e0020-156a-4cc3-b70b-d93eb2961b29'),
	(97,'Entry',1,0,'2017-06-25 13:30:45','2017-06-25 13:30:45','8c9f4262-88ba-434d-9e1e-a2d992d9d436'),
	(98,'Entry',1,0,'2017-06-25 13:38:12','2017-06-25 13:38:12','2c7681ae-2ab6-4381-9fb1-3da14181307e'),
	(99,'Entry',1,0,'2017-06-25 13:40:52','2017-06-25 13:40:52','1b9175ef-8b13-499f-aff8-c6274f5e1eae'),
	(100,'Entry',1,0,'2017-06-25 13:43:34','2017-06-25 13:43:34','8f4d7ae7-5630-41ed-b5b4-25d8ea6946a9'),
	(101,'Entry',1,0,'2017-06-25 13:46:12','2017-06-25 13:46:12','4556f27f-5842-49cb-b256-a9309986005c'),
	(102,'Entry',1,0,'2017-06-25 13:48:46','2017-06-25 13:48:46','7aad3a23-2379-4dd5-9881-1da8473d6860'),
	(103,'Entry',1,0,'2017-06-25 13:48:47','2017-06-25 13:48:47','75558a5e-f904-4fbd-a0d2-1edaf39cef9e'),
	(104,'Entry',1,0,'2017-06-25 13:51:35','2017-06-25 13:51:35','cdcaac2c-a158-4c12-bd6c-5629685d0183'),
	(105,'Entry',1,0,'2017-06-25 13:54:13','2017-06-25 13:54:13','6eb175ee-377d-4a36-9678-1f2143db08c0'),
	(106,'Entry',1,0,'2017-06-25 13:56:46','2017-06-25 13:56:46','9d013e09-f571-4d4b-b2fa-edf4f4e01e1f'),
	(107,'Entry',1,0,'2017-06-25 13:59:13','2017-06-25 13:59:13','c5a18f82-833c-4da3-8eac-79dd8e1b97df'),
	(108,'Entry',1,0,'2017-06-25 14:01:45','2017-06-25 14:01:45','f1b33bd6-ec62-4753-be32-2af0dc959d26'),
	(109,'Entry',1,0,'2017-06-25 14:04:26','2017-06-25 14:04:26','e4629e7c-d752-4602-9466-39e65e686e8c'),
	(110,'Entry',1,0,'2017-06-25 14:09:27','2017-06-25 14:09:27','6813aecc-b910-42f6-a48d-b70cc9ea4141'),
	(111,'Entry',1,0,'2017-06-25 14:12:00','2017-06-25 14:12:00','d8b18aab-c45a-4b7f-88df-ab072535209d'),
	(112,'Entry',1,0,'2017-06-25 14:14:36','2017-06-25 14:14:36','603135ae-cc66-446c-8563-b2ccb991eac9'),
	(113,'Entry',1,0,'2017-06-25 14:19:28','2017-06-25 14:19:28','4cd536af-55d0-49f1-a008-d4b0c3646b44'),
	(114,'Entry',1,0,'2017-06-25 14:22:08','2017-06-25 14:22:08','a3f03702-225c-4224-808c-62886296e36e'),
	(115,'Entry',1,0,'2017-06-25 14:24:37','2017-06-25 14:24:37','2064e939-477b-46a8-b7fd-c2319d4d8bd6'),
	(116,'Entry',1,0,'2017-06-25 14:29:48','2017-06-25 14:29:48','ba2ddde6-dae2-4fdc-8d3d-4997b7290534'),
	(117,'Entry',1,0,'2017-06-25 14:32:46','2017-06-25 14:32:46','3b2ff6ae-5993-4bae-8d7f-5cf790a7fe2e'),
	(118,'Entry',1,0,'2017-06-25 14:35:24','2017-06-25 14:35:24','7bc43933-64be-43d3-b5cb-7492853da2a8'),
	(119,'Entry',1,0,'2017-06-25 14:48:04','2017-06-25 14:48:04','b89d0371-b50e-451c-af47-83b955837e23'),
	(120,'Entry',1,0,'2017-06-25 14:50:40','2017-06-25 14:50:40','afa0ed07-f3e6-4911-ac05-33363cf3c58b'),
	(121,'Entry',1,0,'2017-06-25 14:53:47','2017-06-25 14:53:47','ed088dd7-a2e6-4804-9c82-6ecb4ee04856'),
	(122,'Entry',1,0,'2017-06-25 14:56:36','2017-06-25 14:56:36','f75c5b98-e4cb-4ad8-8197-cbbce705a331'),
	(123,'Entry',1,0,'2017-06-25 15:04:56','2017-06-25 15:04:56','9ac5609f-3570-4905-b7f1-3b5555e89476'),
	(124,'Entry',1,0,'2017-06-25 15:09:59','2017-06-25 15:09:59','e33a7927-8ba4-45e7-99e4-4ec0d1e9ec58'),
	(125,'Entry',1,0,'2017-06-25 15:12:42','2017-06-25 15:12:42','3b1606f3-b214-4b41-956d-6e83c0273466'),
	(126,'Entry',1,0,'2017-06-25 15:18:59','2017-06-25 15:18:59','1d5370ff-a0aa-4edd-b016-fe687ce8deed'),
	(127,'Entry',1,0,'2017-06-25 15:21:42','2017-06-25 15:21:42','c0c5c72e-66de-4ac0-ad95-53bf8328ef0f'),
	(128,'Entry',1,0,'2017-06-25 15:26:33','2017-06-25 15:26:33','302e85cf-e544-4495-9147-06877c27133b'),
	(129,'Entry',1,0,'2017-06-25 15:29:13','2017-06-25 15:29:13','e4584f36-45e0-425e-b27c-9c2edea4ea9f'),
	(130,'Entry',1,0,'2017-06-25 15:33:28','2017-06-25 15:33:28','5810611d-e83c-4216-b073-4b7060912ff6'),
	(131,'Entry',1,0,'2017-06-25 15:36:08','2017-06-25 15:36:08','797d384a-ea6f-4039-a892-bff2c20cf801');

/*!40000 ALTER TABLE `craft_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_elements_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements_i18n`;

CREATE TABLE `craft_elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `craft_elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `craft_elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `craft_elements_i18n_enabled_idx` (`enabled`),
  KEY `craft_elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements_i18n` WRITE;
/*!40000 ALTER TABLE `craft_elements_i18n` DISABLE KEYS */;

INSERT INTO `craft_elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'de','',NULL,1,'2017-06-24 14:06:41','2017-06-25 06:46:53','ec5297d9-1d31-461c-84bb-e7fb7deac05a'),
	(2,2,'de','homepage','__home__',1,'2017-06-24 14:06:46','2017-06-24 14:06:46','28db9446-5454-4ac8-bdba-bd35583d38bd'),
	(15,15,'de','fabian',NULL,1,'2017-06-25 09:34:09','2017-06-25 09:34:09','01a2cc34-bed3-4186-a57f-d2c2f153b560'),
	(16,16,'de','juliette',NULL,1,'2017-06-25 09:34:43','2017-06-25 09:34:43','7c35fff7-ab4a-4ba8-85c9-f153b52828c0'),
	(17,17,'de','joel',NULL,1,'2017-06-25 09:34:53','2017-06-25 09:34:53','81f73c29-3361-49b8-8ff5-6732d7e13b36'),
	(19,19,'de','tobias',NULL,1,'2017-06-25 09:36:47','2017-06-25 09:36:47','48f40738-c9d3-4ca4-9279-9839413283de'),
	(21,21,'de','mustermann',NULL,1,'2017-06-25 09:41:34','2017-06-25 09:41:34','ac33f588-df4b-4f11-ab40-0cbff506d7e1'),
	(22,22,'de','mustermann',NULL,1,'2017-06-25 09:41:43','2017-06-25 09:41:43','2611d529-cc71-457c-87d1-8b8824e81c54'),
	(23,23,'de','musterfrau',NULL,1,'2017-06-25 09:41:51','2017-06-25 09:41:51','fd839571-8476-45c7-89d2-f3959bdaeadc'),
	(24,24,'de','marvin',NULL,1,'2017-06-25 09:42:39','2017-06-25 09:42:39','e9633844-5175-4ff0-9021-d6f9a4212592'),
	(25,25,'de','oliver',NULL,1,'2017-06-25 09:46:59','2017-06-25 09:46:59','ffe00d30-b8fd-4857-a025-b1996f4a9490'),
	(26,26,'de','emely',NULL,1,'2017-06-25 09:52:49','2017-06-25 09:52:49','9a79f795-540f-49de-ae1e-60ee8d73df35'),
	(27,27,'de','emely',NULL,1,'2017-06-25 09:52:56','2017-06-25 09:52:56','38cef0e6-94db-459f-ab83-54110ae728db'),
	(28,28,'de','marie',NULL,1,'2017-06-25 09:56:13','2017-06-25 09:56:13','7e5ed903-4332-4d7b-8935-d932ca7be0a5'),
	(29,29,'de','tim',NULL,1,'2017-06-25 10:03:08','2017-06-25 10:03:08','96f68cd0-77ba-4196-b10d-1a541b16838f'),
	(30,30,'de','dennis',NULL,1,'2017-06-25 10:06:16','2017-06-25 10:06:16','d6ff828d-58c6-41f2-bfd5-702e13187d80'),
	(31,31,'de','vlad',NULL,1,'2017-06-25 10:09:27','2017-06-25 10:09:27','330f9c7e-5637-4267-8999-2b06dd578d0b'),
	(32,32,'de','patrick',NULL,1,'2017-06-25 10:18:52','2017-06-25 10:18:52','636e225c-8daa-4e68-85d1-c421b83b174f'),
	(33,33,'de','marco',NULL,1,'2017-06-25 10:24:15','2017-06-25 10:24:15','35701420-d276-4c65-8be4-f90ef1b61068'),
	(34,34,'de','detlef',NULL,1,'2017-06-25 10:34:07','2017-06-25 10:34:07','0014d79f-f473-45e8-a9a5-e5257b2e54a8'),
	(35,35,'de','yannik',NULL,1,'2017-06-25 10:42:16','2017-06-25 10:42:16','71d1e00c-9476-4bf5-a3d1-1dd83b35c5b1'),
	(36,36,'de','max',NULL,1,'2017-06-25 10:44:59','2017-06-25 10:44:59','c1d3de32-cd32-4e57-8794-48299089d112'),
	(37,37,'de','felix',NULL,1,'2017-06-25 10:47:36','2017-06-25 10:47:36','7b0daf4f-e5b1-4f32-b1d4-9d0fc29479be'),
	(38,38,'de','daniel',NULL,1,'2017-06-25 10:49:56','2017-06-25 10:49:56','2711a816-1c65-47e4-9651-7810049a3c12'),
	(39,39,'de','alex',NULL,1,'2017-06-25 10:52:33','2017-06-25 10:52:33','5c745577-e3a4-4499-86af-d05e8290a804'),
	(40,40,'de','silas',NULL,1,'2017-06-25 10:55:40','2017-06-25 10:55:40','27bf35ba-acf1-42b2-992f-54e0cafb3d57'),
	(41,41,'de','ole',NULL,1,'2017-06-25 10:58:24','2017-06-25 10:58:24','8a3ffe7a-eeb2-4940-8307-5a72191e4f80'),
	(42,42,'de','bian',NULL,1,'2017-06-25 11:01:22','2017-06-25 11:01:22','ad9411f7-b6a9-4409-9de8-96198c5035a2'),
	(43,43,'de','elian',NULL,1,'2017-06-25 11:04:01','2017-06-25 11:04:01','cd8db850-cf3d-45d5-814e-f90e77850dc6'),
	(44,44,'de','tanja',NULL,1,'2017-06-25 11:06:44','2017-06-25 11:06:44','e588cbfd-ed7d-421c-84f2-f6398f11adc6'),
	(45,45,'de','adrian',NULL,1,'2017-06-25 11:09:33','2017-06-25 11:09:33','4c5baa27-6921-456f-945d-8a82db2796c4'),
	(46,46,'de','meliass',NULL,1,'2017-06-25 11:12:16','2017-06-25 11:12:16','16432ac1-f2aa-4cfb-9ff7-2936e48e01a7'),
	(47,47,'de','melissa',NULL,1,'2017-06-25 11:12:29','2017-06-25 11:12:29','6588eafc-3e80-4300-beb3-0d4d31c2196f'),
	(48,48,'de','benedict',NULL,1,'2017-06-25 11:14:54','2017-06-25 11:14:54','ed9995e3-df0b-4215-ba97-efe27518e38b'),
	(49,49,'de','mara',NULL,1,'2017-06-25 11:20:18','2017-06-25 11:20:18','a2ad1a27-3bbc-4bbb-83e6-3e1ccb66441b'),
	(50,50,'de','robin',NULL,1,'2017-06-25 11:23:07','2017-06-25 11:23:07','6ef859dc-5a95-437e-b8ae-52cd729b03c6'),
	(51,51,'de','michael',NULL,1,'2017-06-25 11:26:17','2017-06-25 11:26:17','e1eee861-3e68-4a8f-9d69-40e3ae8893e5'),
	(52,52,'de','alexander',NULL,1,'2017-06-25 11:28:57','2017-06-25 11:28:57','6d0c1bfd-483a-44f0-b79d-4ba949212351'),
	(53,53,'de','waldemar',NULL,1,'2017-06-25 11:34:23','2017-06-25 11:34:23','63bdc73e-0ae8-4ac1-b461-88a1d1bb4b04'),
	(54,54,'de','philipp',NULL,1,'2017-06-25 11:34:41','2017-06-25 11:34:41','6cfec736-486c-476e-843a-58b7671b5f3a'),
	(55,55,'de','jenni',NULL,1,'2017-06-25 11:37:31','2017-06-25 11:37:31','176a9bf1-02b7-4b9a-bdf4-908178d92304'),
	(56,56,'de','max',NULL,1,'2017-06-25 11:39:59','2017-06-25 11:39:59','044c91cb-f4d2-4fbe-b4b4-568d4b324a74'),
	(57,57,'de','jasmin',NULL,1,'2017-06-25 11:42:36','2017-06-25 11:42:36','cb933aa4-22c1-4bef-bacd-16ca68fecb4a'),
	(58,58,'de','luis',NULL,1,'2017-06-25 11:45:17','2017-06-25 11:45:17','616ea912-0b6f-4ace-a3c6-dd367ca0c638'),
	(59,59,'de','nils',NULL,1,'2017-06-25 11:47:49','2017-06-25 11:47:49','7bcdeafd-7dd0-4f65-acb0-3bdca772021a'),
	(60,60,'de','ole',NULL,1,'2017-06-25 11:50:16','2017-06-25 11:50:16','d2224f46-6a8e-4ac1-8a5c-9f5e908a6084'),
	(61,61,'de','fabian',NULL,1,'2017-06-25 11:52:47','2017-06-25 11:52:47','6b86d2de-015e-41b1-b89f-aa0599d507ae'),
	(62,62,'de','justin',NULL,1,'2017-06-25 11:55:28','2017-06-25 11:55:28','be2ca9e2-aa32-46ee-897a-b5f10318d6c9'),
	(63,63,'de','larissa',NULL,1,'2017-06-25 11:57:50','2017-06-25 11:57:50','712277b4-2012-46ed-83f8-0c3b53ecc9d8'),
	(64,64,'de','lenden',NULL,1,'2017-06-25 12:00:29','2017-06-25 12:00:29','035ba863-68ef-4c95-87b0-285e79f2fe8e'),
	(65,65,'de','leonard',NULL,1,'2017-06-25 12:03:12','2017-06-25 12:03:12','c5a13322-7cc5-48c7-95fc-695c86cdc4b9'),
	(66,66,'de','fin',NULL,1,'2017-06-25 12:05:46','2017-06-25 12:05:46','ca553de0-cc37-4875-90c4-4f28dae48b2b'),
	(67,67,'de','jan',NULL,1,'2017-06-25 12:08:21','2017-06-25 12:08:21','8fefcf8a-7691-4bf7-af01-a0adc14cf3bd'),
	(68,68,'de','jan',NULL,1,'2017-06-25 12:10:20','2017-06-25 12:10:20','656728fb-5072-49b4-8d64-c072b9122684'),
	(69,69,'de','michael',NULL,1,'2017-06-25 12:13:00','2017-06-25 12:13:00','dfe58a03-4063-4110-be15-cd430b7dc0fc'),
	(70,70,'de','merle',NULL,1,'2017-06-25 12:15:33','2017-06-25 12:15:33','fe3ab471-3a24-4d91-b313-3046c6df78b9'),
	(71,71,'de','selma',NULL,1,'2017-06-25 12:18:20','2017-06-25 12:18:20','bcbfd451-fdb5-458f-83a5-853fc552d86e'),
	(72,72,'de','luana',NULL,1,'2017-06-25 12:21:01','2017-06-25 12:21:01','f4b2c9ae-33ca-4ecb-8be6-be2471a10a8b'),
	(73,73,'de','emmely',NULL,1,'2017-06-25 12:23:43','2017-06-25 12:23:43','56c0a89e-5f1c-4c3b-a21c-1629c6966706'),
	(74,74,'de','dalleen',NULL,1,'2017-06-25 12:26:21','2017-06-25 12:26:21','c2ec7cf3-3688-41a2-99ef-2dc0cc01063a'),
	(75,75,'de','daniel',NULL,1,'2017-06-25 12:29:15','2017-06-25 12:29:15','0ec68515-e92c-405c-8644-9a3274980c55'),
	(76,76,'de','ben',NULL,1,'2017-06-25 12:32:31','2017-06-25 12:32:31','53137089-d46d-4de9-8ce8-a25976fd9fd3'),
	(77,77,'de','marcel',NULL,1,'2017-06-25 12:35:20','2017-06-25 12:35:20','e19c9c47-ff2f-4b82-a115-0d03a7faf487'),
	(78,78,'de','jamel',NULL,1,'2017-06-25 12:38:23','2017-06-25 12:38:23','946798f2-b8ea-48d1-995a-0cad982e85ba'),
	(79,79,'de','emrulah',NULL,1,'2017-06-25 12:41:35','2017-06-25 12:41:35','78231b23-d157-4cca-82a4-3b0b1626820d'),
	(80,80,'de','matthias',NULL,1,'2017-06-25 12:44:42','2017-06-25 12:44:42','865ecad4-8873-4764-b8d2-3f24bb7350ed'),
	(81,81,'de','david',NULL,1,'2017-06-25 12:47:31','2017-06-25 12:47:31','be2efde2-8c84-40bf-a2c8-edfc9447719e'),
	(82,82,'de','daniele',NULL,1,'2017-06-25 12:50:14','2017-06-25 12:50:14','5e74f55c-3bd7-4e44-bed0-3024b2fd9796'),
	(83,83,'de','landon',NULL,1,'2017-06-25 12:53:03','2017-06-25 12:53:03','d9314dc8-7064-4a4c-80eb-53fe1fc1647a'),
	(84,84,'de','michael',NULL,1,'2017-06-25 12:56:17','2017-06-25 12:56:17','95846191-5056-4623-9d10-540b840870a0'),
	(85,85,'de','jens',NULL,1,'2017-06-25 12:58:09','2017-06-25 12:58:09','e782959f-9b8a-4997-be2c-0dbba9655e92'),
	(86,86,'de','marie',NULL,1,'2017-06-25 13:00:40','2017-06-25 13:00:40','504a973b-a400-4869-9948-0c6b36f9683c'),
	(87,87,'de','thomas',NULL,1,'2017-06-25 13:03:37','2017-06-25 13:03:37','09969cb7-31e3-47dd-bac8-e08df6520e5d'),
	(88,88,'de','hendrik',NULL,1,'2017-06-25 13:06:22','2017-06-25 13:06:22','6b4aeae7-0357-4a13-9a1b-c581482eb1a8'),
	(89,89,'de','alfred',NULL,1,'2017-06-25 13:09:05','2017-06-25 13:09:05','0a2a3b9e-c7f8-453b-bd64-b49bf90117e0'),
	(90,90,'de','michelle',NULL,1,'2017-06-25 13:11:41','2017-06-25 13:11:41','bde10ac2-1e6e-4d3b-b9a2-748df2c8b11f'),
	(91,91,'de','max',NULL,1,'2017-06-25 13:14:12','2017-06-25 13:14:12','ba2c5fa9-9eab-4b79-9a17-b17e80525a78'),
	(92,92,'de','rojin',NULL,1,'2017-06-25 13:17:07','2017-06-25 13:17:07','983d3787-75d0-4eed-8303-02c31bb584be'),
	(93,93,'de','laurenz',NULL,1,'2017-06-25 13:19:48','2017-06-25 13:19:48','10c2c699-bd34-4d84-b9de-f5086a48070f'),
	(94,94,'de','emrullah',NULL,1,'2017-06-25 13:22:27','2017-06-25 13:22:27','92056c64-0c68-44af-a58e-2a7dd8bc6957'),
	(95,95,'de','marie',NULL,1,'2017-06-25 13:25:12','2017-06-25 13:25:12','f1582d1e-34e3-4d8c-b516-6151dabd7fad'),
	(96,96,'de','lena',NULL,1,'2017-06-25 13:27:46','2017-06-25 13:27:46','a908e9a3-f3a9-480f-8896-6d1da9fde442'),
	(97,97,'de','zeynab',NULL,1,'2017-06-25 13:30:45','2017-06-25 13:30:45','e0853051-cedb-4d30-8f95-13afb480ef9f'),
	(98,98,'de','anton',NULL,1,'2017-06-25 13:38:12','2017-06-25 13:38:12','95ef0a45-1029-44d8-a138-75fb4a4b2a46'),
	(99,99,'de','timo',NULL,1,'2017-06-25 13:40:52','2017-06-25 13:40:52','552e8802-4a1b-49f2-b414-9336c99a21eb'),
	(100,100,'de','vinh',NULL,1,'2017-06-25 13:43:34','2017-06-25 13:43:34','341d9118-4bb7-42d6-aba9-4270ce14316f'),
	(101,101,'de','joshua',NULL,1,'2017-06-25 13:46:12','2017-06-25 13:46:12','ef338612-c02f-41cf-85e9-d53f62477f17'),
	(102,102,'de','lukas',NULL,1,'2017-06-25 13:48:46','2017-06-25 13:48:46','1dcc171d-dd20-47e1-84c5-a8a10dd5e758'),
	(103,103,'de','lukas',NULL,1,'2017-06-25 13:48:47','2017-06-25 13:48:47','6562fb19-d77b-4a11-831c-817f63b2b0d3'),
	(104,104,'de','justin',NULL,1,'2017-06-25 13:51:35','2017-06-25 13:51:35','d7cf2742-690b-4a90-9507-fc75a9fb8390'),
	(105,105,'de','peter',NULL,1,'2017-06-25 13:54:13','2017-06-25 13:54:13','969219ad-8540-4877-a01d-c205e6d83890'),
	(106,106,'de','emily',NULL,1,'2017-06-25 13:56:46','2017-06-25 13:56:46','64ecb664-b7b0-4103-aaae-8d3271fd7333'),
	(107,107,'de','darleen',NULL,1,'2017-06-25 13:59:13','2017-06-25 13:59:13','8c3004ea-563a-446e-9b25-3f063a4b75e0'),
	(108,108,'de','alex',NULL,1,'2017-06-25 14:01:45','2017-06-25 14:01:45','ef0aa07c-8934-4254-ba4b-6b2d039d5940'),
	(109,109,'de','tailan',NULL,1,'2017-06-25 14:04:26','2017-06-25 14:04:26','39363aa9-1af9-4c16-84db-da04fc3aefbb'),
	(110,110,'de','kevin',NULL,1,'2017-06-25 14:09:27','2017-06-25 14:09:27','f80ce593-b8ba-4452-bcff-cb28ae7fc0dc'),
	(111,111,'de','kevin',NULL,1,'2017-06-25 14:12:00','2017-06-25 14:12:00','0851392d-7bc0-4881-abcc-770346a81360'),
	(112,112,'de','leonas',NULL,1,'2017-06-25 14:14:36','2017-06-25 14:14:36','bc4ba26f-d9ea-415e-a11e-9bd2ab6845a8'),
	(113,113,'de','jens',NULL,1,'2017-06-25 14:19:28','2017-06-25 14:19:28','a28b38c8-2217-438b-a3f8-4a8bb0ad01e3'),
	(114,114,'de','michael',NULL,1,'2017-06-25 14:22:08','2017-06-25 14:22:08','01c33ea9-bbbd-42b5-9276-394dbbd41f1c'),
	(115,115,'de','marie',NULL,1,'2017-06-25 14:24:37','2017-06-25 14:24:37','1cbcdca0-c11b-4434-873d-b40cfd11c1da'),
	(116,116,'de','oliver',NULL,1,'2017-06-25 14:29:48','2017-06-25 14:29:48','014726be-bc91-44df-8e9c-c88c535475b7'),
	(117,117,'de','kai',NULL,1,'2017-06-25 14:32:46','2017-06-25 14:32:46','35d68c0a-6052-4c38-9f33-ed56abb094f3'),
	(118,118,'de','anna',NULL,1,'2017-06-25 14:35:24','2017-06-25 14:35:24','185c4ef3-bc79-41e6-841e-2a15a3e9aa6d'),
	(119,119,'de','katharina',NULL,1,'2017-06-25 14:48:04','2017-06-25 14:48:04','31f4938d-fa59-40e1-a0fb-3f2502346f6a'),
	(120,120,'de','leonas',NULL,1,'2017-06-25 14:50:40','2017-06-25 14:50:40','21dd5ad1-b696-49e7-bc44-b80d29fa4b57'),
	(121,121,'de','anette',NULL,1,'2017-06-25 14:53:47','2017-06-25 14:53:47','dee7f5e4-8272-469b-963b-d13b20f228e8'),
	(122,122,'de','maik',NULL,1,'2017-06-25 14:56:36','2017-06-25 14:56:36','5e23f512-7c1d-4262-966a-f84109f164cc'),
	(123,123,'de','elsa',NULL,1,'2017-06-25 15:04:56','2017-06-25 15:04:56','b485d007-1823-49e3-ae5c-0e801dbff990'),
	(124,124,'de','peter',NULL,1,'2017-06-25 15:09:59','2017-06-25 15:09:59','8c39940e-1d0f-4ead-bd54-a84c0883d78a'),
	(125,125,'de','teresa',NULL,1,'2017-06-25 15:12:42','2017-06-25 15:12:42','363d7c22-7781-4cc0-983a-cd49a54b2e5d'),
	(126,126,'de','susanna',NULL,1,'2017-06-25 15:18:59','2017-06-25 15:18:59','15ec6b67-1f79-4523-a622-12b27591e31b'),
	(127,127,'de','david',NULL,1,'2017-06-25 15:21:42','2017-06-25 15:21:42','8d3f147d-ceef-4db1-b0bf-840962db7e8b'),
	(128,128,'de','dustin',NULL,1,'2017-06-25 15:26:33','2017-06-25 15:26:33','65cd07c0-4e20-4d66-b596-87d6b7e8db90'),
	(129,129,'de','leon',NULL,1,'2017-06-25 15:29:13','2017-06-25 15:29:13','6bd17e90-d743-4d45-8cc6-ec99fc22d503'),
	(130,130,'de','christoph',NULL,1,'2017-06-25 15:33:28','2017-06-25 15:33:28','37b3f0ee-f6f4-4e5d-9a5d-cb2f563d6a7d'),
	(131,131,'de','ralph',NULL,1,'2017-06-25 15:36:08','2017-06-25 15:36:08','601a6e41-f312-47a5-b1ca-6f171c7ddfda');

/*!40000 ALTER TABLE `craft_elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_emailmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_emailmessages`;

CREATE TABLE `craft_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `craft_emailmessages_locale_fk` (`locale`),
  CONSTRAINT `craft_emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entries`;

CREATE TABLE `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entries_sectionId_idx` (`sectionId`),
  KEY `craft_entries_typeId_idx` (`typeId`),
  KEY `craft_entries_postDate_idx` (`postDate`),
  KEY `craft_entries_expiryDate_idx` (`expiryDate`),
  KEY `craft_entries_authorId_fk` (`authorId`),
  CONSTRAINT `craft_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entries` WRITE;
/*!40000 ALTER TABLE `craft_entries` DISABLE KEYS */;

INSERT INTO `craft_entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,NULL,'2017-06-24 14:06:46',NULL,'2017-06-24 14:06:46','2017-06-24 14:06:46','8d593fe5-9ef6-4035-92f8-c30a6ed5a2c6'),
	(15,3,3,1,'2017-06-25 09:34:09',NULL,'2017-06-25 09:34:09','2017-06-25 09:34:09','2f53db91-0f7b-4210-bdaa-e62da0337817'),
	(16,3,3,1,'2017-06-25 09:34:43',NULL,'2017-06-25 09:34:43','2017-06-25 09:34:43','1268d300-0034-44b8-9dda-781b5bb6e20f'),
	(17,3,3,1,'2017-06-25 09:34:53',NULL,'2017-06-25 09:34:53','2017-06-25 09:34:53','5978959a-e210-4a45-b760-dacc087371c0'),
	(19,3,3,1,'2017-06-25 09:36:47',NULL,'2017-06-25 09:36:47','2017-06-25 09:36:47','8377a870-0e60-47cd-8393-a9b696204aa9'),
	(21,3,3,1,'2017-06-25 09:41:34',NULL,'2017-06-25 09:41:34','2017-06-25 09:41:34','1538bbbb-08ed-44dd-ae1d-ab3c46a612c6'),
	(22,3,3,1,'2017-06-25 09:41:43',NULL,'2017-06-25 09:41:43','2017-06-25 09:41:43','20f2061e-5f21-4b86-9d1d-2e822e6edf2f'),
	(23,3,3,1,'2017-06-25 09:41:51',NULL,'2017-06-25 09:41:51','2017-06-25 09:41:51','85e32c0d-a6e1-41d7-8c22-5b53a92b7955'),
	(24,3,3,1,'2017-06-25 09:42:39',NULL,'2017-06-25 09:42:39','2017-06-25 09:42:39','37df5967-e9c0-4e36-baf3-048c4041d1d1'),
	(25,3,3,1,'2017-06-25 09:46:59',NULL,'2017-06-25 09:46:59','2017-06-25 09:46:59','5b604a87-1e67-4d9e-b7c0-e369d6d01174'),
	(26,3,3,1,'2017-06-25 09:52:49',NULL,'2017-06-25 09:52:50','2017-06-25 09:52:50','81a57637-7504-43f4-9628-39087a53c2ad'),
	(27,3,3,1,'2017-06-25 09:52:56',NULL,'2017-06-25 09:52:56','2017-06-25 09:52:56','d96289b6-5cb7-48bc-a0cc-1d7b138bd6ef'),
	(28,3,3,1,'2017-06-25 09:56:13',NULL,'2017-06-25 09:56:13','2017-06-25 09:56:13','da19bb5e-cc5c-4a19-9855-7baf4b769e66'),
	(29,3,3,1,'2017-06-25 10:03:08',NULL,'2017-06-25 10:03:08','2017-06-25 10:03:08','f73648c2-7a70-4dd0-918b-e4d5c57437c0'),
	(30,3,3,1,'2017-06-25 10:06:16',NULL,'2017-06-25 10:06:16','2017-06-25 10:06:16','868c1fc5-55d8-4e19-9206-0f0066784356'),
	(31,3,3,1,'2017-06-25 10:09:27',NULL,'2017-06-25 10:09:27','2017-06-25 10:09:27','d963debc-b677-40b3-a672-58efcbb6fd4d'),
	(32,3,3,1,'2017-06-25 10:18:52',NULL,'2017-06-25 10:18:52','2017-06-25 10:18:52','e39244e1-57ed-4fca-af54-3a60e2cb2b6b'),
	(33,3,3,1,'2017-06-25 10:24:15',NULL,'2017-06-25 10:24:15','2017-06-25 10:24:15','81b4050c-4a9d-4ba5-829d-eee8377ffe72'),
	(34,3,3,1,'2017-06-25 10:34:07',NULL,'2017-06-25 10:34:07','2017-06-25 10:34:07','3f55a1a6-8bc1-445c-a234-cd86d5ae3ebe'),
	(35,3,3,1,'2017-06-25 10:42:16',NULL,'2017-06-25 10:42:16','2017-06-25 10:42:16','0ba519fd-7a99-457e-b273-0617088d5d96'),
	(36,3,3,1,'2017-06-25 10:44:59',NULL,'2017-06-25 10:44:59','2017-06-25 10:44:59','469dfe33-d49e-4d2d-9dda-aefdc2f874cc'),
	(37,3,3,1,'2017-06-25 10:47:36',NULL,'2017-06-25 10:47:36','2017-06-25 10:47:36','c4889039-d2ca-4e51-bb26-c2b27d3679b1'),
	(38,3,3,1,'2017-06-25 10:49:56',NULL,'2017-06-25 10:49:56','2017-06-25 10:49:56','e2b04fd6-5e08-4f4e-aadf-11e4eeba437d'),
	(39,3,3,1,'2017-06-25 10:52:32',NULL,'2017-06-25 10:52:33','2017-06-25 10:52:33','a21d2607-6716-4ea0-834f-697a1d1608a7'),
	(40,3,3,1,'2017-06-25 10:55:40',NULL,'2017-06-25 10:55:40','2017-06-25 10:55:40','1b1a844d-d638-4a58-8a8a-87f406174b93'),
	(41,3,3,1,'2017-06-25 10:58:23',NULL,'2017-06-25 10:58:24','2017-06-25 10:58:24','79567439-09d2-4b2e-8bb6-8ea85442cd8c'),
	(42,3,3,1,'2017-06-25 11:01:22',NULL,'2017-06-25 11:01:22','2017-06-25 11:01:22','72789200-aefd-4a4b-b718-d3619ec419af'),
	(43,3,3,1,'2017-06-25 11:04:01',NULL,'2017-06-25 11:04:01','2017-06-25 11:04:01','94ff8641-ef0a-426a-bb06-0b0c25707bdd'),
	(44,3,3,1,'2017-06-25 11:06:44',NULL,'2017-06-25 11:06:44','2017-06-25 11:06:44','e7964893-4d84-473e-8603-fc28bb777716'),
	(45,3,3,1,'2017-06-25 11:09:33',NULL,'2017-06-25 11:09:33','2017-06-25 11:09:33','2c4d25b7-5df3-46be-8910-b6c5b9ad8bed'),
	(46,3,3,1,'2017-06-25 11:12:16',NULL,'2017-06-25 11:12:16','2017-06-25 11:12:16','c62cf017-53f6-40f4-b1ea-621ea3191441'),
	(47,3,3,1,'2017-06-25 11:12:29',NULL,'2017-06-25 11:12:29','2017-06-25 11:12:29','230f2b35-26e4-4774-aae5-5a2b2b5955d2'),
	(48,3,3,1,'2017-06-25 11:14:54',NULL,'2017-06-25 11:14:54','2017-06-25 11:14:54','678c3b1f-1239-425a-8995-b34a6c9ea668'),
	(49,3,3,1,'2017-06-25 11:20:18',NULL,'2017-06-25 11:20:18','2017-06-25 11:20:18','f99e2b36-c1b4-4591-b950-3994b3fe6e0f'),
	(50,3,3,1,'2017-06-25 11:23:07',NULL,'2017-06-25 11:23:07','2017-06-25 11:23:07','d0cc0f60-bdb4-4f00-b9a6-4bc21b3187b0'),
	(51,3,3,1,'2017-06-25 11:26:17',NULL,'2017-06-25 11:26:17','2017-06-25 11:26:17','04bf6228-6a6b-4b11-9081-bdaf45475687'),
	(52,3,3,1,'2017-06-25 11:28:57',NULL,'2017-06-25 11:28:57','2017-06-25 11:28:57','be7f0392-d74e-4ee4-bf08-5fc33cd81c9a'),
	(53,3,3,1,'2017-06-25 11:34:23',NULL,'2017-06-25 11:34:24','2017-06-25 11:34:24','dd55849f-11f9-4a07-b0bc-a259f21450f9'),
	(54,3,3,1,'2017-06-25 11:34:41',NULL,'2017-06-25 11:34:41','2017-06-25 11:34:41','2ee189ec-8037-4b17-bbb9-651af341f4c7'),
	(55,3,3,1,'2017-06-25 11:37:31',NULL,'2017-06-25 11:37:31','2017-06-25 11:37:31','f2780bb7-d113-45ce-be37-3fc76e421940'),
	(56,3,3,1,'2017-06-25 11:39:59',NULL,'2017-06-25 11:39:59','2017-06-25 11:39:59','6ae128ef-1df5-4a3f-9953-b6aa2ecef40e'),
	(57,3,3,1,'2017-06-25 11:42:36',NULL,'2017-06-25 11:42:36','2017-06-25 11:42:36','ca26c1f9-93b4-4970-a763-0456143d4bfe'),
	(58,3,3,1,'2017-06-25 11:45:16',NULL,'2017-06-25 11:45:17','2017-06-25 11:45:17','939e9441-ea53-4237-88ae-d07cc1f3105e'),
	(59,3,3,1,'2017-06-25 11:47:49',NULL,'2017-06-25 11:47:49','2017-06-25 11:47:49','588934cc-d0d1-46b4-9e90-3977ada6b953'),
	(60,3,3,1,'2017-06-25 11:50:16',NULL,'2017-06-25 11:50:16','2017-06-25 11:50:16','4a97c0e5-a419-41cb-9ad5-913bc6721ace'),
	(61,3,3,1,'2017-06-25 11:52:47',NULL,'2017-06-25 11:52:47','2017-06-25 11:52:47','29b24d0c-8d81-48b4-995a-5787164ab31b'),
	(62,3,3,1,'2017-06-25 11:55:28',NULL,'2017-06-25 11:55:28','2017-06-25 11:55:28','3f3dde28-d3ed-4254-aac5-bc35364549bf'),
	(63,3,3,1,'2017-06-25 11:57:49',NULL,'2017-06-25 11:57:50','2017-06-25 11:57:50','09f9686c-27e0-411e-b532-95aa473d727f'),
	(64,3,3,1,'2017-06-25 12:00:28',NULL,'2017-06-25 12:00:29','2017-06-25 12:00:29','94c28195-d780-498a-b846-bf0808ef167b'),
	(65,3,3,1,'2017-06-25 12:03:12',NULL,'2017-06-25 12:03:12','2017-06-25 12:03:12','16abe80b-88a6-4b1e-baa1-b480eb6f0898'),
	(66,3,3,1,'2017-06-25 12:05:46',NULL,'2017-06-25 12:05:46','2017-06-25 12:05:46','5e195abf-fa41-472f-b357-55301429c61b'),
	(67,3,3,1,'2017-06-25 12:08:21',NULL,'2017-06-25 12:08:21','2017-06-25 12:08:21','097002ab-5532-4615-8457-10836cc94d71'),
	(68,3,3,1,'2017-06-25 12:10:20',NULL,'2017-06-25 12:10:20','2017-06-25 12:10:20','85cbaf2f-fc74-451f-95cb-02303fa599ea'),
	(69,3,3,1,'2017-06-25 12:13:00',NULL,'2017-06-25 12:13:00','2017-06-25 12:13:00','a837ff40-2278-4e5d-8bd9-cce01920d344'),
	(70,3,3,1,'2017-06-25 12:15:33',NULL,'2017-06-25 12:15:33','2017-06-25 12:15:33','7a28157b-b9f5-486a-ba5c-24f0b69417bb'),
	(71,3,3,1,'2017-06-25 12:18:19',NULL,'2017-06-25 12:18:20','2017-06-25 12:18:20','5f158c7a-72de-46de-ac48-e86bc790c9fd'),
	(72,3,3,1,'2017-06-25 12:21:01',NULL,'2017-06-25 12:21:01','2017-06-25 12:21:01','6bcf8b91-ed05-445d-b5cd-eec5252cf09b'),
	(73,3,3,1,'2017-06-25 12:23:43',NULL,'2017-06-25 12:23:43','2017-06-25 12:23:43','19d0757a-c664-4e79-9fc6-1317ab31db8e'),
	(74,3,3,1,'2017-06-25 12:26:21',NULL,'2017-06-25 12:26:21','2017-06-25 12:26:21','51b3beef-1d59-43cf-8d0a-0d8b1b159b64'),
	(75,3,3,1,'2017-06-25 12:29:15',NULL,'2017-06-25 12:29:15','2017-06-25 12:29:15','2d26ea38-11cd-4c0b-a5b8-17ba7b149c7e'),
	(76,3,3,1,'2017-06-25 12:32:31',NULL,'2017-06-25 12:32:31','2017-06-25 12:32:31','4f98f7b5-3760-4108-a49e-c68d02c09598'),
	(77,3,3,1,'2017-06-25 12:35:20',NULL,'2017-06-25 12:35:20','2017-06-25 12:35:20','daa6bd01-3948-4f1a-8c96-208ee743aede'),
	(78,3,3,1,'2017-06-25 12:38:23',NULL,'2017-06-25 12:38:23','2017-06-25 12:38:23','86a399f5-7bdb-4fa1-8626-9874a8fdd22c'),
	(79,3,3,1,'2017-06-25 12:41:35',NULL,'2017-06-25 12:41:35','2017-06-25 12:41:35','b0828272-9783-4772-9405-3b8dad0b6a45'),
	(80,3,3,1,'2017-06-25 12:44:42',NULL,'2017-06-25 12:44:42','2017-06-25 12:44:42','3f3d9150-6d54-4200-8bfe-13bcf6b94dc4'),
	(81,3,3,1,'2017-06-25 12:47:31',NULL,'2017-06-25 12:47:31','2017-06-25 12:47:31','02e2cfe4-38fd-4260-93f0-265fe24c3bd5'),
	(82,3,3,1,'2017-06-25 12:50:14',NULL,'2017-06-25 12:50:14','2017-06-25 12:50:14','700ac00a-b7f1-48f8-b30f-f738df297b33'),
	(83,3,3,1,'2017-06-25 12:53:03',NULL,'2017-06-25 12:53:03','2017-06-25 12:53:03','8cf81c35-64a9-490c-af27-c4230f0a8c9b'),
	(84,3,3,1,'2017-06-25 12:56:17',NULL,'2017-06-25 12:56:17','2017-06-25 12:56:17','a6ecb02c-aa09-4605-ad3a-26cfce6d39df'),
	(85,3,3,1,'2017-06-25 12:58:09',NULL,'2017-06-25 12:58:09','2017-06-25 12:58:09','3b749cfe-6f43-4a3e-85da-5782cf7874de'),
	(86,3,3,1,'2017-06-25 13:00:40',NULL,'2017-06-25 13:00:40','2017-06-25 13:00:40','dbc1567c-1161-4bb4-8a0f-a73f399999d9'),
	(87,3,3,1,'2017-06-25 13:03:37',NULL,'2017-06-25 13:03:37','2017-06-25 13:03:37','7777315d-7b22-4d38-8657-4058c5544ddd'),
	(88,3,3,1,'2017-06-25 13:06:22',NULL,'2017-06-25 13:06:22','2017-06-25 13:06:22','6f7cb16a-472a-487b-b61a-acae0183435c'),
	(89,3,3,1,'2017-06-25 13:09:05',NULL,'2017-06-25 13:09:05','2017-06-25 13:09:05','72faafdb-6c30-4869-aa41-98e02508bee0'),
	(90,3,3,1,'2017-06-25 13:11:41',NULL,'2017-06-25 13:11:41','2017-06-25 13:11:41','a0184a03-74cc-4bc0-9e4d-7c5718848742'),
	(91,3,3,1,'2017-06-25 13:14:12',NULL,'2017-06-25 13:14:12','2017-06-25 13:14:12','f73b22ef-1237-4b36-8cca-df5754307d5e'),
	(92,3,3,1,'2017-06-25 13:17:07',NULL,'2017-06-25 13:17:07','2017-06-25 13:17:07','6be50252-5fc0-49f0-86a1-cdbd5d212e99'),
	(93,3,3,1,'2017-06-25 13:19:48',NULL,'2017-06-25 13:19:48','2017-06-25 13:19:48','c5e8c86a-136f-454d-a5e7-41e996858537'),
	(94,3,3,1,'2017-06-25 13:22:27',NULL,'2017-06-25 13:22:27','2017-06-25 13:22:27','c649406d-72fd-4c53-87aa-f994fccf4c7f'),
	(95,3,3,1,'2017-06-25 13:25:12',NULL,'2017-06-25 13:25:12','2017-06-25 13:25:12','3d84ee89-3dac-4017-b68e-d34f8cc5948d'),
	(96,3,3,1,'2017-06-25 13:27:46',NULL,'2017-06-25 13:27:46','2017-06-25 13:27:46','867397d4-fa18-4488-a68f-d2643cdecdb1'),
	(97,3,3,1,'2017-06-25 13:30:45',NULL,'2017-06-25 13:30:45','2017-06-25 13:30:45','18217bd6-c8f9-4954-a5ae-a15e5b1f692d'),
	(98,3,3,1,'2017-06-25 13:38:12',NULL,'2017-06-25 13:38:12','2017-06-25 13:38:12','1560d62d-efa8-4fa8-a41a-b6b60954d796'),
	(99,3,3,1,'2017-06-25 13:40:52',NULL,'2017-06-25 13:40:52','2017-06-25 13:40:52','39182628-5d1f-40ac-8f26-6685268d9243'),
	(100,3,3,1,'2017-06-25 13:43:34',NULL,'2017-06-25 13:43:34','2017-06-25 13:43:34','fd74adae-d1d9-4e98-b102-2b9717520890'),
	(101,3,3,1,'2017-06-25 13:46:11',NULL,'2017-06-25 13:46:12','2017-06-25 13:46:12','0bdb3759-d508-4a59-a21c-fc5c2b237fa7'),
	(102,3,3,1,'2017-06-25 13:48:46',NULL,'2017-06-25 13:48:47','2017-06-25 13:48:47','4153d152-503f-450b-9975-6ff3f60b1252'),
	(103,3,3,1,'2017-06-25 13:48:47',NULL,'2017-06-25 13:48:47','2017-06-25 13:48:47','4366052b-42c8-4c03-92c8-3e5ec8c609fb'),
	(104,3,3,1,'2017-06-25 13:51:35',NULL,'2017-06-25 13:51:36','2017-06-25 13:51:36','050d331f-99d2-4f60-a7d4-9a063f7c9d42'),
	(105,3,3,1,'2017-06-25 13:54:13',NULL,'2017-06-25 13:54:13','2017-06-25 13:54:13','207f68f3-01f8-4eab-ae66-1b7dbcbef782'),
	(106,3,3,1,'2017-06-25 13:56:46',NULL,'2017-06-25 13:56:46','2017-06-25 13:56:46','ee6f8b4f-593b-408c-9ac4-f244bcea5fa3'),
	(107,3,3,1,'2017-06-25 13:59:13',NULL,'2017-06-25 13:59:13','2017-06-25 13:59:13','2c40363f-4a03-409e-b09e-fc0808c23209'),
	(108,3,3,1,'2017-06-25 14:01:45',NULL,'2017-06-25 14:01:45','2017-06-25 14:01:45','0cbdfd57-d115-497e-ab22-98ce24187ed6'),
	(109,3,3,1,'2017-06-25 14:04:26',NULL,'2017-06-25 14:04:26','2017-06-25 14:04:26','ab7ad502-9e4d-4e1a-86a1-98a5132e81ce'),
	(110,3,3,1,'2017-06-25 14:09:27',NULL,'2017-06-25 14:09:27','2017-06-25 14:09:27','589a441b-d3d5-4fa8-8abf-ae63c66e23c6'),
	(111,3,3,1,'2017-06-25 14:12:00',NULL,'2017-06-25 14:12:00','2017-06-25 14:12:00','2a1ccbc9-dc4e-47d2-821b-dfa51857c28c'),
	(112,3,3,1,'2017-06-25 14:14:35',NULL,'2017-06-25 14:14:36','2017-06-25 14:14:36','2b8bb71f-16cd-47ef-b122-e58a99c2661b'),
	(113,3,3,1,'2017-06-25 14:19:28',NULL,'2017-06-25 14:19:28','2017-06-25 14:19:28','a218ccde-5312-470f-b7d9-b22340353fc2'),
	(114,3,3,1,'2017-06-25 14:22:08',NULL,'2017-06-25 14:22:08','2017-06-25 14:22:08','6cce613e-b7b7-48d7-915d-d48b5fdaf8fb'),
	(115,3,3,1,'2017-06-25 14:24:37',NULL,'2017-06-25 14:24:37','2017-06-25 14:24:37','5f75d83e-b39b-4bf7-81c7-dd2a28e5fa14'),
	(116,3,3,1,'2017-06-25 14:29:48',NULL,'2017-06-25 14:29:48','2017-06-25 14:29:48','c0ffe704-2c6d-4626-8691-2c14eae6bff7'),
	(117,3,3,1,'2017-06-25 14:32:46',NULL,'2017-06-25 14:32:46','2017-06-25 14:32:46','e81db756-ebfc-4c33-a191-0eb5940db6bf'),
	(118,3,3,1,'2017-06-25 14:35:24',NULL,'2017-06-25 14:35:24','2017-06-25 14:35:24','d9a1b216-c033-4588-8772-6a348a2716d9'),
	(119,3,3,1,'2017-06-25 14:48:04',NULL,'2017-06-25 14:48:04','2017-06-25 14:48:04','86db2566-35d7-4c43-8f48-1bdc1855a10a'),
	(120,3,3,1,'2017-06-25 14:50:40',NULL,'2017-06-25 14:50:40','2017-06-25 14:50:40','ba95e4cc-178b-4b38-a221-36bd1da23c8d'),
	(121,3,3,1,'2017-06-25 14:53:47',NULL,'2017-06-25 14:53:47','2017-06-25 14:53:47','3624a2ef-af3f-4eec-b20e-5146ede945cb'),
	(122,3,3,1,'2017-06-25 14:56:36',NULL,'2017-06-25 14:56:36','2017-06-25 14:56:36','b8a106f9-0d29-4455-bb0a-76378dc9c7af'),
	(123,3,3,1,'2017-06-25 15:04:56',NULL,'2017-06-25 15:04:56','2017-06-25 15:04:56','b55b578f-ec8f-4497-9c7d-0bb6ee8791a7'),
	(124,3,3,1,'2017-06-25 15:09:59',NULL,'2017-06-25 15:09:59','2017-06-25 15:09:59','ca31752f-de8a-44d1-8242-3e3fcc99370c'),
	(125,3,3,1,'2017-06-25 15:12:42',NULL,'2017-06-25 15:12:42','2017-06-25 15:12:42','dd89e278-6605-448f-9eec-2a86c355e1d9'),
	(126,3,3,1,'2017-06-25 15:18:59',NULL,'2017-06-25 15:18:59','2017-06-25 15:18:59','12d4e3ed-2ed8-4eb7-b694-73fec3526d23'),
	(127,3,3,1,'2017-06-25 15:21:42',NULL,'2017-06-25 15:21:42','2017-06-25 15:21:42','3dcb1c9d-7e0c-467b-9d15-0a87ea0f7e83'),
	(128,3,3,1,'2017-06-25 15:26:33',NULL,'2017-06-25 15:26:33','2017-06-25 15:26:33','bbbbc834-ea49-42c8-ab75-a2bcf6076de2'),
	(129,3,3,1,'2017-06-25 15:29:12',NULL,'2017-06-25 15:29:13','2017-06-25 15:29:13','af50b47c-f976-43af-864c-e1c66d232040'),
	(130,3,3,1,'2017-06-25 15:33:28',NULL,'2017-06-25 15:33:28','2017-06-25 15:33:28','d697dd27-823b-419b-beb2-53865d5c36a7'),
	(131,3,3,1,'2017-06-25 15:36:08',NULL,'2017-06-25 15:36:08','2017-06-25 15:36:08','dad95a81-e534-44b8-997a-603f0ce40d9f');

/*!40000 ALTER TABLE `craft_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrydrafts`;

CREATE TABLE `craft_entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entrydrafts_sectionId_fk` (`sectionId`),
  KEY `craft_entrydrafts_creatorId_fk` (`creatorId`),
  KEY `craft_entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `craft_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrytypes`;

CREATE TABLE `craft_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `craft_entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `craft_entrytypes_sectionId_fk` (`sectionId`),
  KEY `craft_entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entrytypes` WRITE;
/*!40000 ALTER TABLE `craft_entrytypes` DISABLE KEYS */;

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,3,'Homepage','homepage',1,'Title',NULL,1,'2017-06-24 14:06:46','2017-06-24 14:06:46','2c46cdf6-40b2-49fe-975f-486feb866969'),
	(3,3,10,'Spieler','spieler',0,NULL,'{spielername}',1,'2017-06-24 14:27:25','2017-06-24 14:27:54','95402216-1364-42e9-9be8-a5a70f890eac');

/*!40000 ALTER TABLE `craft_entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entryversions`;

CREATE TABLE `craft_entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entryversions_sectionId_fk` (`sectionId`),
  KEY `craft_entryversions_creatorId_fk` (`creatorId`),
  KEY `craft_entryversions_locale_fk` (`locale`),
  CONSTRAINT `craft_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entryversions` WRITE;
/*!40000 ALTER TABLE `craft_entryversions` DISABLE KEYS */;

INSERT INTO `craft_entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,1,1,'de',1,NULL,'{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1498313206,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2017-06-24 14:06:46','2017-06-24 14:06:46','00d1e1fb-8f4c-4862-ba8e-ef4ffd605b00'),
	(2,2,1,1,'de',2,NULL,'{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Bussimulator.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1498313206,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Bussimulator.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2017-06-24 14:06:46','2017-06-24 14:06:46','1c310511-2360-497e-828b-c555a2d58152'),
	(17,15,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Fabian\",\"slug\":\"fabian\",\"postDate\":1498383249,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Fabian\"}}','2017-06-25 09:34:09','2017-06-25 09:34:09','34ad67be-bad4-4dd0-a525-c9273dc3fcd5'),
	(18,16,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Juliette\",\"slug\":\"juliette\",\"postDate\":1498383283,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"18\",\"5\":\"Juliette\"}}','2017-06-25 09:34:43','2017-06-25 09:34:43','206c0855-1d3d-473f-a9bb-1cc6cfbe6fa1'),
	(19,17,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Joel\",\"slug\":\"joel\",\"postDate\":1498383293,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"25\",\"5\":\"Joel\"}}','2017-06-25 09:34:53','2017-06-25 09:34:53','9f080cfe-0f63-4636-a7e0-e1ee6937e591'),
	(21,19,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Tobias\",\"slug\":\"tobias\",\"postDate\":1498383407,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"12\",\"5\":\"Tobias\"}}','2017-06-25 09:36:47','2017-06-25 09:36:47','2095ce4b-fbf8-42c3-8957-ac0418b5dbd3'),
	(23,21,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Mustermann\",\"slug\":\"mustermann\",\"postDate\":1498383694,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"39\",\"5\":\"Mustermann\"}}','2017-06-25 09:41:34','2017-06-25 09:41:34','29308e39-fd7a-4b28-842a-684271e8616b'),
	(24,22,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Mustermann\",\"slug\":\"mustermann\",\"postDate\":1498383703,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"25\",\"5\":\"Mustermann\"}}','2017-06-25 09:41:43','2017-06-25 09:41:43','9ae28062-7bc7-44cc-8ca3-89150028b1cd'),
	(25,23,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Musterfrau\",\"slug\":\"musterfrau\",\"postDate\":1498383711,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"11\",\"5\":\"Musterfrau\"}}','2017-06-25 09:41:51','2017-06-25 09:41:51','29417fe5-e632-410b-88a5-92fa7a3d5603'),
	(26,24,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marvin\",\"slug\":\"marvin\",\"postDate\":1498383759,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"44\",\"5\":\"Marvin\"}}','2017-06-25 09:42:39','2017-06-25 09:42:39','9c08cee4-a076-4dec-b7c8-d6b9792700e0'),
	(27,25,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Oliver\",\"slug\":\"oliver\",\"postDate\":1498384019,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Oliver\"}}','2017-06-25 09:46:59','2017-06-25 09:46:59','21f3c291-0a5b-4435-83d0-2169bfa8ab7d'),
	(28,26,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Emely\",\"slug\":\"emely\",\"postDate\":1498384369,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"5\",\"5\":\"Emely\"}}','2017-06-25 09:52:50','2017-06-25 09:52:50','ed2f5d2a-9887-4571-82da-acb5875127bc'),
	(29,27,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Emely\",\"slug\":\"emely\",\"postDate\":1498384376,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"5\",\"5\":\"Emely\"}}','2017-06-25 09:52:56','2017-06-25 09:52:56','2a341b94-8554-4b6c-82e5-2a18fc87ae9d'),
	(30,28,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marie\",\"slug\":\"marie\",\"postDate\":1498384573,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Marie\"}}','2017-06-25 09:56:13','2017-06-25 09:56:13','5da33ff6-91eb-46f2-a70e-78a4870c416c'),
	(31,29,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Tim\",\"slug\":\"tim\",\"postDate\":1498384988,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"62\",\"5\":\"Tim\"}}','2017-06-25 10:03:08','2017-06-25 10:03:08','9a19983e-287d-4d64-abbf-220b12b4f104'),
	(32,30,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Dennis\",\"slug\":\"dennis\",\"postDate\":1498385176,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"69\",\"5\":\"Dennis\"}}','2017-06-25 10:06:16','2017-06-25 10:06:16','4e53ee3e-c734-44be-aa79-5fe41a3f810b'),
	(33,31,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Vlad\",\"slug\":\"vlad\",\"postDate\":1498385367,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"45\",\"5\":\"Vlad\"}}','2017-06-25 10:09:27','2017-06-25 10:09:27','ee0193fd-ffeb-4a25-aae0-7f02b61f1900'),
	(34,32,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Patrick\",\"slug\":\"patrick\",\"postDate\":1498385932,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"41\",\"5\":\"Patrick\"}}','2017-06-25 10:18:52','2017-06-25 10:18:52','f1f7e04b-5e0d-48a1-a016-7789b1bafc77'),
	(35,33,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marco\",\"slug\":\"marco\",\"postDate\":1498386255,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"38\",\"5\":\"Marco\"}}','2017-06-25 10:24:15','2017-06-25 10:24:15','ffdb8480-6fc5-4c7e-8bff-9613479286ae'),
	(36,34,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Detlef\",\"slug\":\"detlef\",\"postDate\":1498386847,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"47\",\"5\":\"Detlef\"}}','2017-06-25 10:34:07','2017-06-25 10:34:07','a453b5cb-e633-41cf-a1a8-a6c8d7cee6e5'),
	(37,35,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Yannik\",\"slug\":\"yannik\",\"postDate\":1498387336,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"18\",\"5\":\"Yannik\"}}','2017-06-25 10:42:16','2017-06-25 10:42:16','3c355e4e-7d92-4ce6-b984-cdab22fc6582'),
	(38,36,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Max\",\"slug\":\"max\",\"postDate\":1498387499,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"42\",\"5\":\"Max\"}}','2017-06-25 10:44:59','2017-06-25 10:44:59','430997f9-6fbc-4e7e-977a-3c9a4fee3ea6'),
	(39,37,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Felix\",\"slug\":\"felix\",\"postDate\":1498387656,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Felix\"}}','2017-06-25 10:47:36','2017-06-25 10:47:36','168d6a96-d7de-4c59-a3c2-56736032ed5f'),
	(40,38,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Daniel\",\"slug\":\"daniel\",\"postDate\":1498387796,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"73\",\"5\":\"Daniel\"}}','2017-06-25 10:49:56','2017-06-25 10:49:56','2613cff1-2c35-4330-b259-30f347028155'),
	(41,39,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Alex\",\"slug\":\"alex\",\"postDate\":1498387952,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"73\",\"5\":\"Alex\"}}','2017-06-25 10:52:33','2017-06-25 10:52:33','07ac60a9-cbff-4506-9ac7-9af52d3d3f6f'),
	(42,40,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Silas\",\"slug\":\"silas\",\"postDate\":1498388140,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"18\",\"5\":\"Silas\"}}','2017-06-25 10:55:40','2017-06-25 10:55:40','29ae0e15-d775-43c0-a6ef-2df5850e9446'),
	(43,41,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Ole\",\"slug\":\"ole\",\"postDate\":1498388303,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"32\",\"5\":\"Ole\"}}','2017-06-25 10:58:24','2017-06-25 10:58:24','92113330-d51b-45b5-bbac-ceaf73f6299a'),
	(44,42,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Bian\",\"slug\":\"bian\",\"postDate\":1498388482,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Bian\"}}','2017-06-25 11:01:22','2017-06-25 11:01:22','92fd88e6-6817-46c0-b180-d0ef7147db4f'),
	(45,43,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Elian\",\"slug\":\"elian\",\"postDate\":1498388641,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"68\",\"5\":\"Elian\"}}','2017-06-25 11:04:01','2017-06-25 11:04:01','1870ecd9-ee17-47e0-8bfc-7f5e87ec5452'),
	(46,44,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Tanja\",\"slug\":\"tanja\",\"postDate\":1498388804,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Tanja\"}}','2017-06-25 11:06:44','2017-06-25 11:06:44','2373fec5-52a7-47b4-9fa3-722cfea4730b'),
	(47,45,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Adrian\",\"slug\":\"adrian\",\"postDate\":1498388973,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"73\",\"5\":\"Adrian\"}}','2017-06-25 11:09:33','2017-06-25 11:09:33','a073e24e-24fd-47f8-8a09-c93b1547895f'),
	(48,46,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Meliass\",\"slug\":\"meliass\",\"postDate\":1498389136,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"\",\"5\":\"Meliass\"}}','2017-06-25 11:12:16','2017-06-25 11:12:16','6cb39843-439a-4ac8-a70f-732772cae6e8'),
	(49,47,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Melissa\",\"slug\":\"melissa\",\"postDate\":1498389149,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Melissa\"}}','2017-06-25 11:12:29','2017-06-25 11:12:29','5fdf3873-a752-41e3-93d8-1119bfbdd58d'),
	(50,48,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Benedict\",\"slug\":\"benedict\",\"postDate\":1498389294,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"54\",\"5\":\"Benedict\"}}','2017-06-25 11:14:54','2017-06-25 11:14:54','b5661f8e-1248-4fa3-8eea-f902d80e6f81'),
	(51,49,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Mara\",\"slug\":\"mara\",\"postDate\":1498389618,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"45\",\"5\":\"Mara\"}}','2017-06-25 11:20:18','2017-06-25 11:20:18','4664cf1f-c355-454c-850b-e848aeba34fd'),
	(52,50,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Robin\",\"slug\":\"robin\",\"postDate\":1498389787,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"56\",\"5\":\"Robin\"}}','2017-06-25 11:23:07','2017-06-25 11:23:07','5de3cfd9-b326-4e3c-a2fa-e954c2aa7956'),
	(53,51,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Michael\",\"slug\":\"michael\",\"postDate\":1498389977,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Michael\"}}','2017-06-25 11:26:17','2017-06-25 11:26:17','725e5606-c15a-43a7-a508-4fae43d0e67c'),
	(54,52,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Alexander\",\"slug\":\"alexander\",\"postDate\":1498390137,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"42\",\"5\":\"Alexander\"}}','2017-06-25 11:28:57','2017-06-25 11:28:57','546fead9-bd2c-435b-bf24-154d275e572b'),
	(55,53,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Waldemar\",\"slug\":\"waldemar\",\"postDate\":1498390463,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"11\",\"5\":\"Waldemar\"}}','2017-06-25 11:34:24','2017-06-25 11:34:24','9171700d-79c2-49d4-9054-f55c7b1dfab1'),
	(56,54,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Philipp\",\"slug\":\"philipp\",\"postDate\":1498390481,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"38\",\"5\":\"Philipp\"}}','2017-06-25 11:34:41','2017-06-25 11:34:41','9c7a2b95-bb05-4192-9ad5-61bf36fbf695'),
	(57,55,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jenni\",\"slug\":\"jenni\",\"postDate\":1498390651,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"62\",\"5\":\"Jenni\"}}','2017-06-25 11:37:31','2017-06-25 11:37:31','6b2cfbb9-0dde-47a1-9410-2e321fe09d8b'),
	(58,56,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Max\",\"slug\":\"max\",\"postDate\":1498390799,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"24\",\"5\":\"Max\"}}','2017-06-25 11:39:59','2017-06-25 11:39:59','8b2c580e-75ce-4daf-aa7a-66b5f9e7ba1c'),
	(59,57,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jasmin\",\"slug\":\"jasmin\",\"postDate\":1498390956,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"54\",\"5\":\"Jasmin\"}}','2017-06-25 11:42:36','2017-06-25 11:42:36','586d55eb-c41b-4d3c-affe-f1af3942477a'),
	(60,58,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Luis\",\"slug\":\"luis\",\"postDate\":1498391116,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"34\",\"5\":\"Luis\"}}','2017-06-25 11:45:17','2017-06-25 11:45:17','e27d92d4-31c8-4f3b-b6e7-97bca4b7e121'),
	(61,59,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Nils\",\"slug\":\"nils\",\"postDate\":1498391269,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Nils\"}}','2017-06-25 11:47:49','2017-06-25 11:47:49','80a62937-5fcb-4bd2-8052-db8308768299'),
	(62,60,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Ole\",\"slug\":\"ole\",\"postDate\":1498391416,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"97\",\"5\":\"Ole\"}}','2017-06-25 11:50:16','2017-06-25 11:50:16','39e96d63-e5df-4646-8ced-e4dc8a043fb0'),
	(63,61,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Fabian\",\"slug\":\"fabian\",\"postDate\":1498391567,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Fabian\"}}','2017-06-25 11:52:47','2017-06-25 11:52:47','afdb558f-bb36-46c3-ab3e-a5ad3579a72b'),
	(64,62,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Justin\",\"slug\":\"justin\",\"postDate\":1498391728,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Justin\"}}','2017-06-25 11:55:28','2017-06-25 11:55:28','40502b39-3070-4d4b-8ff9-84012bba1584'),
	(65,63,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Larissa\",\"slug\":\"larissa\",\"postDate\":1498391869,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"5\",\"5\":\"Larissa\"}}','2017-06-25 11:57:50','2017-06-25 11:57:50','210a8a7d-402c-4e96-a31c-76bfc06b97eb'),
	(66,64,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Lenden\",\"slug\":\"lenden\",\"postDate\":1498392028,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"73\",\"5\":\"Lenden\"}}','2017-06-25 12:00:29','2017-06-25 12:00:29','2d0881dd-e3f8-4aed-896a-fe93cbe9bc01'),
	(67,65,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Leonard\",\"slug\":\"leonard\",\"postDate\":1498392192,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"38\",\"5\":\"Leonard\"}}','2017-06-25 12:03:12','2017-06-25 12:03:12','a6f66fe6-1f16-49db-9861-ad2e33968d37'),
	(68,66,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Fin\",\"slug\":\"fin\",\"postDate\":1498392346,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Fin\"}}','2017-06-25 12:05:46','2017-06-25 12:05:46','5eb8e52f-2541-4d4e-833b-43fa79a2291d'),
	(69,67,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jan\",\"slug\":\"jan\",\"postDate\":1498392501,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"7\",\"5\":\"Jan\"}}','2017-06-25 12:08:21','2017-06-25 12:08:21','2d393b74-2949-4806-8684-59c1956bc3aa'),
	(70,68,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jan\",\"slug\":\"jan\",\"postDate\":1498392620,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"44\",\"5\":\"Jan\"}}','2017-06-25 12:10:20','2017-06-25 12:10:20','f7021028-74f9-4b50-aa69-785dfd58ff7a'),
	(71,69,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Michael\",\"slug\":\"michael\",\"postDate\":1498392780,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"12\",\"5\":\"Michael\"}}','2017-06-25 12:13:00','2017-06-25 12:13:00','05d15385-5fdc-4ae8-9fd1-ebbbde91140c'),
	(72,70,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Merle\",\"slug\":\"merle\",\"postDate\":1498392933,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"34\",\"5\":\"Merle\"}}','2017-06-25 12:15:33','2017-06-25 12:15:33','c1eec232-fd51-473a-ac68-000b58e4eb23'),
	(73,71,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Selma\",\"slug\":\"selma\",\"postDate\":1498393099,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Selma\"}}','2017-06-25 12:18:20','2017-06-25 12:18:20','0975941f-2820-41be-a77e-d2c129443d94'),
	(74,72,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Luana\",\"slug\":\"luana\",\"postDate\":1498393261,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Luana\"}}','2017-06-25 12:21:01','2017-06-25 12:21:01','05697f76-204c-4a97-ba52-0f706f86cd44'),
	(75,73,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Emmely\",\"slug\":\"emmely\",\"postDate\":1498393423,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"56\",\"5\":\"Emmely\"}}','2017-06-25 12:23:43','2017-06-25 12:23:43','0426a498-2254-4427-9537-4acb487461e3'),
	(76,74,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Dalleen\",\"slug\":\"dalleen\",\"postDate\":1498393581,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Dalleen\"}}','2017-06-25 12:26:21','2017-06-25 12:26:21','3b6f4da5-8521-47e2-9e31-e20386d6e933'),
	(77,75,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Daniel\",\"slug\":\"daniel\",\"postDate\":1498393755,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Daniel\"}}','2017-06-25 12:29:15','2017-06-25 12:29:15','f831d15f-6cbe-42a1-a4a2-2bbb28552385'),
	(78,76,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Ben\",\"slug\":\"ben\",\"postDate\":1498393951,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"32\",\"5\":\"Ben\"}}','2017-06-25 12:32:31','2017-06-25 12:32:31','0993df7b-9945-4fbd-944d-e5c0ed79b9ff'),
	(79,77,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marcel\",\"slug\":\"marcel\",\"postDate\":1498394120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Marcel\"}}','2017-06-25 12:35:20','2017-06-25 12:35:20','1fe93e78-2c77-4660-86ea-b1524659a804'),
	(80,78,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jamel\",\"slug\":\"jamel\",\"postDate\":1498394303,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"18\",\"5\":\"Jamel\"}}','2017-06-25 12:38:23','2017-06-25 12:38:23','9f0edb51-4b48-4c4c-8be7-81fca52445b8'),
	(81,79,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Emrulah\",\"slug\":\"emrulah\",\"postDate\":1498394495,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"40\",\"5\":\"Emrulah\"}}','2017-06-25 12:41:35','2017-06-25 12:41:35','ea842728-51a7-4754-aa89-1991b222a852'),
	(82,80,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Matthias\",\"slug\":\"matthias\",\"postDate\":1498394682,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Matthias\"}}','2017-06-25 12:44:42','2017-06-25 12:44:42','079f711d-84b5-4db2-8f9f-fb5e0669dcc2'),
	(83,81,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"David\",\"slug\":\"david\",\"postDate\":1498394851,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"11\",\"5\":\"David\"}}','2017-06-25 12:47:31','2017-06-25 12:47:31','07f9e027-c0cd-4c5e-b79b-a17501f45d8e'),
	(84,82,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Daniele\",\"slug\":\"daniele\",\"postDate\":1498395014,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"13\",\"5\":\"Daniele\"}}','2017-06-25 12:50:14','2017-06-25 12:50:14','edd29c15-9747-4c0a-b897-f4712793bd10'),
	(85,83,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Landon\",\"slug\":\"landon\",\"postDate\":1498395183,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"44\",\"5\":\"Landon\"}}','2017-06-25 12:53:03','2017-06-25 12:53:03','8eb72bfa-0cad-4259-8343-81149a3fedab'),
	(86,84,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Michael\",\"slug\":\"michael\",\"postDate\":1498395377,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"33\",\"5\":\"Michael\"}}','2017-06-25 12:56:17','2017-06-25 12:56:17','e51fe73d-37e7-4d7c-ab16-21ab8668e1bc'),
	(87,85,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jens\",\"slug\":\"jens\",\"postDate\":1498395489,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"0\",\"5\":\"Jens\"}}','2017-06-25 12:58:09','2017-06-25 12:58:09','afa7cfe8-e84c-4db9-978a-65c4c08644b7'),
	(88,86,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marie\",\"slug\":\"marie\",\"postDate\":1498395640,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"38\",\"5\":\"Marie\"}}','2017-06-25 13:00:40','2017-06-25 13:00:40','de54ba71-db92-4250-887b-14544b5694e7'),
	(89,87,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Thomas\",\"slug\":\"thomas\",\"postDate\":1498395817,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"69\",\"5\":\"Thomas\"}}','2017-06-25 13:03:37','2017-06-25 13:03:37','4cf77a96-9e73-4616-81a9-0ef58cabab74'),
	(90,88,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Hendrik\",\"slug\":\"hendrik\",\"postDate\":1498395982,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"52\",\"5\":\"Hendrik\"}}','2017-06-25 13:06:22','2017-06-25 13:06:22','2d9fee09-044e-4cfa-8910-357c7de47a80'),
	(91,89,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Alfred\",\"slug\":\"alfred\",\"postDate\":1498396145,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"12\",\"5\":\"Alfred\"}}','2017-06-25 13:09:05','2017-06-25 13:09:05','536c232e-ec2a-4f78-8828-101dc176eae8'),
	(92,90,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Michelle\",\"slug\":\"michelle\",\"postDate\":1498396301,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Michelle\"}}','2017-06-25 13:11:41','2017-06-25 13:11:41','67d68026-8bdc-4bac-8e39-4179bffa5bba'),
	(93,91,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Max\",\"slug\":\"max\",\"postDate\":1498396452,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"69\",\"5\":\"Max\"}}','2017-06-25 13:14:12','2017-06-25 13:14:12','e5f35eff-efc4-4c21-9bfd-6884f6084ae0'),
	(94,92,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Rojin\",\"slug\":\"rojin\",\"postDate\":1498396627,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"23\",\"5\":\"Rojin\"}}','2017-06-25 13:17:07','2017-06-25 13:17:07','43cef5aa-3653-4e07-8fc2-a4595664b3b2'),
	(95,93,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Laurenz\",\"slug\":\"laurenz\",\"postDate\":1498396788,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"41\",\"5\":\"Laurenz\"}}','2017-06-25 13:19:48','2017-06-25 13:19:48','96f01ba8-2c43-4029-b302-7164505354aa'),
	(96,94,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Emrullah\",\"slug\":\"emrullah\",\"postDate\":1498396947,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"40\",\"5\":\"Emrullah\"}}','2017-06-25 13:22:27','2017-06-25 13:22:27','f1150c24-c176-474c-9e63-d3071e4c7b0e'),
	(97,95,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marie\",\"slug\":\"marie\",\"postDate\":1498397112,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"42\",\"5\":\"Marie\"}}','2017-06-25 13:25:12','2017-06-25 13:25:12','0ba35a81-a580-45ab-b09d-37f5134c329a'),
	(98,96,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Lena\",\"slug\":\"lena\",\"postDate\":1498397266,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"54\",\"5\":\"Lena\"}}','2017-06-25 13:27:47','2017-06-25 13:27:47','28af066a-2592-400d-a267-d0e684328226'),
	(99,97,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Zeynab\",\"slug\":\"zeynab\",\"postDate\":1498397445,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Zeynab\"}}','2017-06-25 13:30:45','2017-06-25 13:30:45','8bea0325-f2ae-4a21-817d-49894095f1aa'),
	(100,98,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Anton\",\"slug\":\"anton\",\"postDate\":1498397892,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"20\",\"5\":\"Anton\"}}','2017-06-25 13:38:12','2017-06-25 13:38:12','2f12a44d-0031-4bc7-a937-173ece1f1aef'),
	(101,99,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Timo\",\"slug\":\"timo\",\"postDate\":1498398052,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"15\",\"5\":\"Timo\"}}','2017-06-25 13:40:52','2017-06-25 13:40:52','8bd3622b-4d3d-4786-aec9-0524b6b2f7c6'),
	(102,100,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Vinh\",\"slug\":\"vinh\",\"postDate\":1498398214,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"42\",\"5\":\"Vinh\"}}','2017-06-25 13:43:34','2017-06-25 13:43:34','92238f30-d3f9-4231-95a4-a585600d1672'),
	(103,101,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Joshua\",\"slug\":\"joshua\",\"postDate\":1498398371,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"57\",\"5\":\"Joshua\"}}','2017-06-25 13:46:12','2017-06-25 13:46:12','d4357a76-0c7c-46a9-af5e-490eda7da246'),
	(104,102,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Lukas\",\"slug\":\"lukas\",\"postDate\":1498398526,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Lukas\"}}','2017-06-25 13:48:47','2017-06-25 13:48:47','d6f1261a-86cc-47cf-a26b-b3fad63dc991'),
	(105,103,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Lukas\",\"slug\":\"lukas\",\"postDate\":1498398527,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Lukas\"}}','2017-06-25 13:48:47','2017-06-25 13:48:47','ac1e2f3c-0748-4967-b379-890c91e0726e'),
	(106,104,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Justin\",\"slug\":\"justin\",\"postDate\":1498398695,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"60\",\"5\":\"Justin\"}}','2017-06-25 13:51:36','2017-06-25 13:51:36','d0a1476e-f0cb-4b84-86c7-afa2e567806e'),
	(107,105,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Peter\",\"slug\":\"peter\",\"postDate\":1498398853,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Peter\"}}','2017-06-25 13:54:13','2017-06-25 13:54:13','705e5006-c4b8-4ab5-8b33-3ff9b9e14c21'),
	(108,106,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Emily\",\"slug\":\"emily\",\"postDate\":1498399006,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"56\",\"5\":\"Emily\"}}','2017-06-25 13:56:46','2017-06-25 13:56:46','d800cdd7-3fed-4e9f-ba61-c034d3d1b35f'),
	(109,107,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Darleen\",\"slug\":\"darleen\",\"postDate\":1498399153,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"52\",\"5\":\"Darleen\"}}','2017-06-25 13:59:13','2017-06-25 13:59:13','84aa2340-945a-4b58-9bd6-1ea03cf2c44c'),
	(110,108,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Alex\",\"slug\":\"alex\",\"postDate\":1498399305,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Alex\"}}','2017-06-25 14:01:45','2017-06-25 14:01:45','9cf1646a-c8f1-4d06-b9f8-b05464cac3f4'),
	(111,109,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Tailan\",\"slug\":\"tailan\",\"postDate\":1498399466,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"11\",\"5\":\"Tailan\"}}','2017-06-25 14:04:26','2017-06-25 14:04:26','5a0288c4-eeab-4caa-9ba6-a74ddec47a52'),
	(112,110,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Kevin\",\"slug\":\"kevin\",\"postDate\":1498399767,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Kevin\"}}','2017-06-25 14:09:27','2017-06-25 14:09:27','2c4b5a9b-a439-4a39-84df-4c2bacee32ab'),
	(113,111,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Kevin\",\"slug\":\"kevin\",\"postDate\":1498399920,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Kevin\"}}','2017-06-25 14:12:00','2017-06-25 14:12:00','d6581ffa-5461-40bd-9de1-6e3b26d1ed7f'),
	(114,112,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Leonas\",\"slug\":\"leonas\",\"postDate\":1498400075,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"20\",\"5\":\"Leonas\"}}','2017-06-25 14:14:36','2017-06-25 14:14:36','d00e9a3e-a4d2-405f-bc3f-4067cd3ebda3'),
	(115,113,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Jens\",\"slug\":\"jens\",\"postDate\":1498400368,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"37\",\"5\":\"Jens\"}}','2017-06-25 14:19:28','2017-06-25 14:19:28','40acfede-b597-4dba-a434-2e6e0604fad0'),
	(116,114,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Michael\",\"slug\":\"michael\",\"postDate\":1498400528,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"45\",\"5\":\"Michael\"}}','2017-06-25 14:22:08','2017-06-25 14:22:08','3643d4ba-75bf-423d-b328-d870b90d6d51'),
	(117,115,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Marie\",\"slug\":\"marie\",\"postDate\":1498400677,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"34\",\"5\":\"Marie\"}}','2017-06-25 14:24:37','2017-06-25 14:24:37','b1cac8c7-5c0c-43a0-8454-13be98bd778f'),
	(118,116,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Oliver\",\"slug\":\"oliver\",\"postDate\":1498400988,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"34\",\"5\":\"Oliver\"}}','2017-06-25 14:29:48','2017-06-25 14:29:48','ba6f2d81-fd54-4de3-a6f5-89e7385c1d08'),
	(119,117,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Kai\",\"slug\":\"kai\",\"postDate\":1498401166,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"18\",\"5\":\"Kai\"}}','2017-06-25 14:32:46','2017-06-25 14:32:46','ce08a4f8-f8fa-42f5-b921-0d7133683453'),
	(120,118,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Anna\",\"slug\":\"anna\",\"postDate\":1498401324,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"12\",\"5\":\"Anna\"}}','2017-06-25 14:35:24','2017-06-25 14:35:24','2d4eba89-a91a-4459-ac7f-5efee4a8723a'),
	(121,119,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Katharina\",\"slug\":\"katharina\",\"postDate\":1498402084,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"39\",\"5\":\"Katharina\"}}','2017-06-25 14:48:04','2017-06-25 14:48:04','9d0d13ae-a1b1-4a07-9ab0-c38597b377b5'),
	(122,120,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Leonas\",\"slug\":\"leonas\",\"postDate\":1498402240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Leonas\"}}','2017-06-25 14:50:40','2017-06-25 14:50:40','206a237e-bcf2-45e8-ba47-6c6aef4a43e1'),
	(123,121,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Anette\",\"slug\":\"anette\",\"postDate\":1498402427,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"43\",\"5\":\"Anette\"}}','2017-06-25 14:53:47','2017-06-25 14:53:47','4f4e725a-63f7-45b5-81a6-1fb67eb17087'),
	(124,122,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Maik\",\"slug\":\"maik\",\"postDate\":1498402596,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"38\",\"5\":\"Maik\"}}','2017-06-25 14:56:36','2017-06-25 14:56:36','dbc01f43-fcf4-468f-9b9d-53095e38674a'),
	(125,123,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Elsa\",\"slug\":\"elsa\",\"postDate\":1498403096,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"36\",\"5\":\"Elsa\"}}','2017-06-25 15:04:56','2017-06-25 15:04:56','081cab34-ef20-40dd-a4d8-38a0da5b4d0d'),
	(126,124,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Peter\",\"slug\":\"peter\",\"postDate\":1498403399,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"49\",\"5\":\"Peter\"}}','2017-06-25 15:09:59','2017-06-25 15:09:59','3abb05d4-eaa1-4f6c-ac76-09afab4e3737'),
	(127,125,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Teresa\",\"slug\":\"teresa\",\"postDate\":1498403562,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"6\",\"5\":\"Teresa\"}}','2017-06-25 15:12:42','2017-06-25 15:12:42','9efa8d95-09d0-4110-a017-2c872d997f58'),
	(128,126,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Susanna\",\"slug\":\"susanna\",\"postDate\":1498403939,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"5\",\"5\":\"Susanna\"}}','2017-06-25 15:18:59','2017-06-25 15:18:59','96fb5b72-3d45-42fb-9fde-d41701607d2d'),
	(129,127,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"David\",\"slug\":\"david\",\"postDate\":1498404102,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"39\",\"5\":\"David\"}}','2017-06-25 15:21:42','2017-06-25 15:21:42','20916faa-ccda-40d6-bc59-8ad1b9ed20ed'),
	(130,128,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Dustin\",\"slug\":\"dustin\",\"postDate\":1498404393,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"38\",\"5\":\"Dustin\"}}','2017-06-25 15:26:33','2017-06-25 15:26:33','28e227b6-c772-4873-8b4e-df6506256583'),
	(131,129,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Leon\",\"slug\":\"leon\",\"postDate\":1498404552,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"56\",\"5\":\"Leon\"}}','2017-06-25 15:29:13','2017-06-25 15:29:13','f4b521d2-bdd9-4cde-b6b0-5a6d041e4585'),
	(132,130,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Christoph\",\"slug\":\"christoph\",\"postDate\":1498404808,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"53\",\"5\":\"Christoph\"}}','2017-06-25 15:33:28','2017-06-25 15:33:28','00156b94-6734-4972-9968-5f68e537c00e'),
	(133,131,3,1,'de',1,NULL,'{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Ralph\",\"slug\":\"ralph\",\"postDate\":1498404968,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"4\":\"46\",\"5\":\"Ralph\"}}','2017-06-25 15:36:08','2017-06-25 15:36:08','43753aa5-bf0b-4a6a-8c4f-2330d6286cc5');

/*!40000 ALTER TABLE `craft_entryversions` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldgroups`;

CREATE TABLE `craft_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldgroups` WRITE;
/*!40000 ALTER TABLE `craft_fieldgroups` DISABLE KEYS */;

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','2017-06-24 14:06:45','2017-06-24 14:06:45','b230bd62-abd2-43da-8094-5fef0acd10e3');

/*!40000 ALTER TABLE `craft_fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayoutfields`;

CREATE TABLE `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `craft_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `craft_fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `craft_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,3,1,1,1,1,'2017-06-24 14:06:46','2017-06-24 14:06:46','e468b752-e9a4-4509-b471-6d9caff02212'),
	(8,10,5,5,0,1,'2017-06-24 14:27:54','2017-06-24 14:27:54','e9a5490d-a122-4ae2-8958-7cd3fbddf86d'),
	(9,10,5,4,0,2,'2017-06-24 14:27:54','2017-06-24 14:27:54','b6bae3cc-9623-4335-a1c4-887e3a093661');

/*!40000 ALTER TABLE `craft_fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouts`;

CREATE TABLE `craft_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouts` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouts` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Tag','2017-06-24 14:06:45','2017-06-24 14:06:45','8a8ce6f1-9351-4bbd-8552-a940c8a40d4e'),
	(3,'Entry','2017-06-24 14:06:46','2017-06-24 14:06:46','0ceffe54-6887-499c-bfd1-a9da492c4201'),
	(10,'Entry','2017-06-24 14:27:54','2017-06-24 14:27:54','f76edae9-fc3d-44d3-afdd-e11b602fd778');

/*!40000 ALTER TABLE `craft_fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouttabs`;

CREATE TABLE `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `craft_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,3,'Content',1,'2017-06-24 14:06:46','2017-06-24 14:06:46','78a19f09-4070-46a7-baa8-036f6452b3a0'),
	(5,10,'Spielerdaten',1,'2017-06-24 14:27:54','2017-06-24 14:27:54','7800de15-7882-4ee7-81c0-0d60a5be2180');

/*!40000 ALTER TABLE `craft_fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fields`;

CREATE TABLE `craft_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `craft_fields_context_idx` (`context`),
  KEY `craft_fields_groupId_fk` (`groupId`),
  CONSTRAINT `craft_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fields` WRITE;
/*!40000 ALTER TABLE `craft_fields` DISABLE KEYS */;

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2017-06-24 14:06:45','2017-06-24 14:06:45','51da4357-1f92-4108-a1ea-317c8d1c7c36'),
	(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2017-06-24 14:06:45','2017-06-24 14:06:45','a9efe530-c0b3-4290-a186-68087ec3c27a'),
	(4,1,'Score','score','global','',0,'Number','{\"min\":\"0\",\"max\":\"\",\"decimals\":\"0\"}','2017-06-24 14:09:25','2017-06-24 14:09:25','aac9cdfb-8c55-4206-b52f-eb172e0dc500'),
	(5,1,'Spielername','spielername','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-06-24 14:25:48','2017-06-24 14:25:48','cc8c6ba4-e2c0-4d16-9313-3cf564747398');

/*!40000 ALTER TABLE `craft_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_globalsets`;

CREATE TABLE `craft_globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `craft_globalsets_handle_unq_idx` (`handle`),
  KEY `craft_globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_info`;

CREATE TABLE `craft_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_info` WRITE;
/*!40000 ALTER TABLE `craft_info` DISABLE KEYS */;

INSERT INTO `craft_info` (`id`, `version`, `schemaVersion`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'2.6.2983','2.6.9',0,'Bussimulator','http://bussimulator.craft.dev','UTC',1,0,'2017-06-24 14:06:38','2017-06-24 14:06:38','7dd7b4f7-107e-4e1f-86c9-3236de1e55da');

/*!40000 ALTER TABLE `craft_info` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_locales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_locales`;

CREATE TABLE `craft_locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `craft_locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_locales` WRITE;
/*!40000 ALTER TABLE `craft_locales` DISABLE KEYS */;

INSERT INTO `craft_locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	('de',1,'2017-06-24 14:06:38','2017-06-24 14:06:38','c59ca878-840a-44c3-9971-b9068042fb3a');

/*!40000 ALTER TABLE `craft_locales` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocks`;

CREATE TABLE `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_matrixblocks_ownerId_idx` (`ownerId`),
  KEY `craft_matrixblocks_fieldId_idx` (`fieldId`),
  KEY `craft_matrixblocks_typeId_idx` (`typeId`),
  KEY `craft_matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `craft_matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `craft_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocktypes`;

CREATE TABLE `craft_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `craft_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_migrations`;

CREATE TABLE `craft_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_migrations_version_unq_idx` (`version`),
  KEY `craft_migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `craft_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `craft_plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_migrations` WRITE;
/*!40000 ALTER TABLE `craft_migrations` DISABLE KEYS */;

INSERT INTO `craft_migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'m000000_000000_base','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','f78c917d-dfc7-46ab-b14a-adb394b97f12'),
	(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','8c558709-8c12-4270-a1b9-dc1282ad7639'),
	(3,NULL,'m140815_000001_add_format_to_transforms','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','d4c49038-2894-4240-847a-2e1a4babef37'),
	(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','b4167104-3cde-4b4e-9e78-5d645d68bce5'),
	(5,NULL,'m140829_000001_single_title_formats','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','259b6b11-806c-4604-b9f1-a0ada71fc617'),
	(6,NULL,'m140831_000001_extended_cache_keys','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','312f3440-03c7-48a3-8b27-440d183aa35e'),
	(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','9b984b41-803a-4504-9c1e-5c28150264a4'),
	(8,NULL,'m141008_000001_elements_index_tune','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','bb31780a-950d-4eb9-ab9f-d7e40a03d54c'),
	(9,NULL,'m141009_000001_assets_source_handle','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','c9e5b428-7ec5-4bd7-b3c4-d70b82ddcf3b'),
	(10,NULL,'m141024_000001_field_layout_tabs','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','8c4b725f-6c20-4704-8dca-b733a710919d'),
	(11,NULL,'m141030_000000_plugin_schema_versions','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','19024a93-e416-4995-bf1b-55ed36723fae'),
	(12,NULL,'m141030_000001_drop_structure_move_permission','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','2a2b2fdd-f4cd-4aeb-bcc0-1e2a55c1df97'),
	(13,NULL,'m141103_000001_tag_titles','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','676a9697-38d1-4299-9732-588682a98ea4'),
	(14,NULL,'m141109_000001_user_status_shuffle','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','5d7f1dd9-6c70-48f6-85d5-e81228c2e059'),
	(15,NULL,'m141126_000001_user_week_start_day','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','740c4002-2957-434b-8435-242e1a416a7a'),
	(16,NULL,'m150210_000001_adjust_user_photo_size','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','d9f0c296-4a09-47f2-87ff-0f0956094dff'),
	(17,NULL,'m150724_000001_adjust_quality_settings','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','ad278dc6-2f80-4b30-9d39-6bf4d98b29f9'),
	(18,NULL,'m150827_000000_element_index_settings','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','9e27bb1d-3a0c-4ce9-ac58-0166b15ed8cd'),
	(19,NULL,'m150918_000001_add_colspan_to_widgets','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','0776fa20-426e-4cb7-8817-d58a884e9342'),
	(20,NULL,'m151007_000000_clear_asset_caches','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','c5455568-9c43-4849-8721-bca0758878ce'),
	(21,NULL,'m151109_000000_text_url_formats','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','2fa3ce40-dba2-4ba6-9599-496f42bb3599'),
	(22,NULL,'m151110_000000_move_logo','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','4349c8b3-6a18-4def-b5a7-3c344fdfa937'),
	(23,NULL,'m151117_000000_adjust_image_widthheight','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','074c4967-858a-429d-8192-9261c432a37d'),
	(24,NULL,'m151127_000000_clear_license_key_status','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','5e6bad01-103d-4166-9c65-5b60a7c9a1b3'),
	(25,NULL,'m151127_000000_plugin_license_keys','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','81de0651-a36a-4c4f-b590-0510bbaaa66d'),
	(26,NULL,'m151130_000000_update_pt_widget_feeds','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','5f5975e6-22f2-4460-9fb1-a2a4ae23559f'),
	(27,NULL,'m160114_000000_asset_sources_public_url_default_true','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','e1b7e09c-f9b8-4f69-87ae-e0564297e589'),
	(28,NULL,'m160223_000000_sortorder_to_smallint','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','cd95b170-420d-4fa0-b468-e69a726bbfe1'),
	(29,NULL,'m160229_000000_set_default_entry_statuses','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','ac855372-370c-45ac-8104-0102e1415ec4'),
	(30,NULL,'m160304_000000_client_permissions','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','7afa68be-e01e-42e6-a9a4-a5ce22aead65'),
	(31,NULL,'m160322_000000_asset_filesize','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','aa77fff7-2a64-4d24-9140-9cce4f712319'),
	(32,NULL,'m160503_000000_orphaned_fieldlayouts','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','1d21dbbb-a991-4ccd-85e5-fca77409d678'),
	(33,NULL,'m160510_000000_tasksettings','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','928cc6b2-aa37-4f36-9dcf-f0ae32b7f5fb'),
	(34,NULL,'m160829_000000_pending_user_content_cleanup','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','28421f5f-fb4b-4e3e-a5c0-a4986742684a'),
	(35,NULL,'m160830_000000_asset_index_uri_increase','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','c9c459d3-34b6-40c4-bea5-ade6ec903905'),
	(36,NULL,'m160919_000000_usergroup_handle_title_unique','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','7b65179e-11b7-4814-9aaf-1567fe7078b7'),
	(37,NULL,'m161108_000000_new_version_format','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','a7af3260-89ad-4a4d-ba63-1bd8d15dbad8'),
	(38,NULL,'m161109_000000_index_shuffle','2017-06-24 14:06:38','2017-06-24 14:06:38','2017-06-24 14:06:38','9ab37309-6d6f-4999-b706-51a6a2e82b4e');

/*!40000 ALTER TABLE `craft_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_plugins`;

CREATE TABLE `craft_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_plugins` WRITE;
/*!40000 ALTER TABLE `craft_plugins` DISABLE KEYS */;

INSERT INTO `craft_plugins` (`id`, `class`, `version`, `schemaVersion`, `licenseKey`, `licenseKeyStatus`, `enabled`, `settings`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,'ElementApi','1.6.0','1.0.0',NULL,'unknown',1,NULL,'2017-06-24 15:40:26','2017-06-24 15:40:26','2017-06-24 15:40:26','7d568dcb-9828-4738-99dc-9a1ffb5400ec');

/*!40000 ALTER TABLE `craft_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_rackspaceaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_rackspaceaccess`;

CREATE TABLE `craft_rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_relations`;

CREATE TABLE `craft_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `craft_relations_sourceId_fk` (`sourceId`),
  KEY `craft_relations_sourceLocale_fk` (`sourceLocale`),
  KEY `craft_relations_targetId_fk` (`targetId`),
  CONSTRAINT `craft_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_routes`;

CREATE TABLE `craft_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `craft_routes_locale_idx` (`locale`),
  CONSTRAINT `craft_routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_searchindex`;

CREATE TABLE `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `craft_searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_searchindex` WRITE;
/*!40000 ALTER TABLE `craft_searchindex` DISABLE KEYS */;

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`)
VALUES
	(1,'username',0,'de',' info kvs de '),
	(1,'firstname',0,'de',''),
	(1,'lastname',0,'de',''),
	(1,'fullname',0,'de',''),
	(1,'email',0,'de',' info kvs de '),
	(1,'slug',0,'de',''),
	(2,'slug',0,'de',' homepage '),
	(2,'title',0,'de',' welcome to bussimulator craft dev '),
	(2,'field',1,'de',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon bussimulator craft dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(21,'field',5,'de',' mustermann '),
	(19,'slug',0,'de',' tobias '),
	(19,'title',0,'de',' tobias '),
	(21,'field',4,'de',' 39 '),
	(21,'slug',0,'de',' mustermann '),
	(21,'title',0,'de',' mustermann '),
	(22,'field',5,'de',' mustermann '),
	(22,'field',4,'de',' 25 '),
	(22,'slug',0,'de',' mustermann '),
	(22,'title',0,'de',' mustermann '),
	(23,'field',5,'de',' musterfrau '),
	(19,'field',4,'de',' 12 '),
	(19,'field',5,'de',' tobias '),
	(23,'field',4,'de',' 11 '),
	(23,'slug',0,'de',' musterfrau '),
	(23,'title',0,'de',' musterfrau '),
	(24,'field',5,'de',' marvin '),
	(17,'title',0,'de',' joel '),
	(17,'slug',0,'de',' joel '),
	(17,'field',4,'de',' 25 '),
	(17,'field',5,'de',' joel '),
	(16,'title',0,'de',' juliette '),
	(16,'slug',0,'de',' juliette '),
	(16,'field',4,'de',' 18 '),
	(16,'field',5,'de',' juliette '),
	(15,'title',0,'de',' fabian '),
	(15,'slug',0,'de',' fabian '),
	(15,'field',4,'de',' 49 '),
	(15,'field',5,'de',' fabian '),
	(24,'field',4,'de',' 44 '),
	(24,'slug',0,'de',' marvin '),
	(24,'title',0,'de',' marvin '),
	(25,'field',5,'de',' oliver '),
	(25,'field',4,'de',' 36 '),
	(25,'slug',0,'de',' oliver '),
	(25,'title',0,'de',' oliver '),
	(26,'field',5,'de',' emely '),
	(26,'field',4,'de',' 5 '),
	(26,'slug',0,'de',' emely '),
	(26,'title',0,'de',' emely '),
	(27,'field',5,'de',' emely '),
	(27,'field',4,'de',' 5 '),
	(27,'slug',0,'de',' emely '),
	(27,'title',0,'de',' emely '),
	(28,'field',5,'de',' marie '),
	(28,'field',4,'de',' 13 '),
	(28,'slug',0,'de',' marie '),
	(28,'title',0,'de',' marie '),
	(29,'field',5,'de',' tim '),
	(29,'field',4,'de',' 62 '),
	(29,'slug',0,'de',' tim '),
	(29,'title',0,'de',' tim '),
	(30,'field',5,'de',' dennis '),
	(30,'field',4,'de',' 69 '),
	(30,'slug',0,'de',' dennis '),
	(30,'title',0,'de',' dennis '),
	(31,'field',5,'de',' vlad '),
	(31,'field',4,'de',' 45 '),
	(31,'slug',0,'de',' vlad '),
	(31,'title',0,'de',' vlad '),
	(32,'field',5,'de',' patrick '),
	(32,'field',4,'de',' 41 '),
	(32,'slug',0,'de',' patrick '),
	(32,'title',0,'de',' patrick '),
	(33,'field',5,'de',' marco '),
	(33,'field',4,'de',' 38 '),
	(33,'slug',0,'de',' marco '),
	(33,'title',0,'de',' marco '),
	(34,'field',5,'de',' detlef '),
	(34,'field',4,'de',' 47 '),
	(34,'slug',0,'de',' detlef '),
	(34,'title',0,'de',' detlef '),
	(35,'field',5,'de',' yannik '),
	(35,'field',4,'de',' 18 '),
	(35,'slug',0,'de',' yannik '),
	(35,'title',0,'de',' yannik '),
	(36,'field',5,'de',' max '),
	(36,'field',4,'de',' 42 '),
	(36,'slug',0,'de',' max '),
	(36,'title',0,'de',' max '),
	(37,'field',5,'de',' felix '),
	(37,'field',4,'de',' 49 '),
	(37,'slug',0,'de',' felix '),
	(37,'title',0,'de',' felix '),
	(38,'field',5,'de',' daniel '),
	(38,'field',4,'de',' 73 '),
	(38,'slug',0,'de',' daniel '),
	(38,'title',0,'de',' daniel '),
	(39,'field',5,'de',' alex '),
	(39,'field',4,'de',' 73 '),
	(39,'slug',0,'de',' alex '),
	(39,'title',0,'de',' alex '),
	(40,'field',5,'de',' silas '),
	(40,'field',4,'de',' 18 '),
	(40,'slug',0,'de',' silas '),
	(40,'title',0,'de',' silas '),
	(41,'field',5,'de',' ole '),
	(41,'field',4,'de',' 32 '),
	(41,'slug',0,'de',' ole '),
	(41,'title',0,'de',' ole '),
	(42,'field',5,'de',' bian '),
	(42,'field',4,'de',' 36 '),
	(42,'slug',0,'de',' bian '),
	(42,'title',0,'de',' bian '),
	(43,'field',5,'de',' elian '),
	(43,'field',4,'de',' 68 '),
	(43,'slug',0,'de',' elian '),
	(43,'title',0,'de',' elian '),
	(44,'field',5,'de',' tanja '),
	(44,'field',4,'de',' 13 '),
	(44,'slug',0,'de',' tanja '),
	(44,'title',0,'de',' tanja '),
	(45,'field',5,'de',' adrian '),
	(45,'field',4,'de',' 73 '),
	(45,'slug',0,'de',' adrian '),
	(45,'title',0,'de',' adrian '),
	(46,'field',5,'de',' meliass '),
	(46,'field',4,'de',' 0 '),
	(46,'slug',0,'de',' meliass '),
	(46,'title',0,'de',' meliass '),
	(47,'field',5,'de',' melissa '),
	(47,'field',4,'de',' 36 '),
	(47,'slug',0,'de',' melissa '),
	(47,'title',0,'de',' melissa '),
	(48,'field',5,'de',' benedict '),
	(48,'field',4,'de',' 54 '),
	(48,'slug',0,'de',' benedict '),
	(48,'title',0,'de',' benedict '),
	(49,'field',5,'de',' mara '),
	(49,'field',4,'de',' 45 '),
	(49,'slug',0,'de',' mara '),
	(49,'title',0,'de',' mara '),
	(50,'field',5,'de',' robin '),
	(50,'field',4,'de',' 56 '),
	(50,'slug',0,'de',' robin '),
	(50,'title',0,'de',' robin '),
	(51,'field',5,'de',' michael '),
	(51,'field',4,'de',' 36 '),
	(51,'slug',0,'de',' michael '),
	(51,'title',0,'de',' michael '),
	(52,'field',5,'de',' alexander '),
	(52,'field',4,'de',' 42 '),
	(52,'slug',0,'de',' alexander '),
	(52,'title',0,'de',' alexander '),
	(53,'field',5,'de',' waldemar '),
	(53,'field',4,'de',' 11 '),
	(53,'slug',0,'de',' waldemar '),
	(53,'title',0,'de',' waldemar '),
	(54,'field',5,'de',' philipp '),
	(54,'field',4,'de',' 38 '),
	(54,'slug',0,'de',' philipp '),
	(54,'title',0,'de',' philipp '),
	(55,'field',5,'de',' jenni '),
	(55,'field',4,'de',' 62 '),
	(55,'slug',0,'de',' jenni '),
	(55,'title',0,'de',' jenni '),
	(56,'field',5,'de',' max '),
	(56,'field',4,'de',' 24 '),
	(56,'slug',0,'de',' max '),
	(56,'title',0,'de',' max '),
	(57,'field',5,'de',' jasmin '),
	(57,'field',4,'de',' 54 '),
	(57,'slug',0,'de',' jasmin '),
	(57,'title',0,'de',' jasmin '),
	(58,'field',5,'de',' luis '),
	(58,'field',4,'de',' 34 '),
	(58,'slug',0,'de',' luis '),
	(58,'title',0,'de',' luis '),
	(59,'field',5,'de',' nils '),
	(59,'field',4,'de',' 49 '),
	(59,'slug',0,'de',' nils '),
	(59,'title',0,'de',' nils '),
	(60,'field',5,'de',' ole '),
	(60,'field',4,'de',' 97 '),
	(60,'slug',0,'de',' ole '),
	(60,'title',0,'de',' ole '),
	(61,'field',5,'de',' fabian '),
	(61,'field',4,'de',' 49 '),
	(61,'slug',0,'de',' fabian '),
	(61,'title',0,'de',' fabian '),
	(62,'field',5,'de',' justin '),
	(62,'field',4,'de',' 13 '),
	(62,'slug',0,'de',' justin '),
	(62,'title',0,'de',' justin '),
	(63,'field',5,'de',' larissa '),
	(63,'field',4,'de',' 5 '),
	(63,'slug',0,'de',' larissa '),
	(63,'title',0,'de',' larissa '),
	(64,'field',5,'de',' lenden '),
	(64,'field',4,'de',' 73 '),
	(64,'slug',0,'de',' lenden '),
	(64,'title',0,'de',' lenden '),
	(65,'field',5,'de',' leonard '),
	(65,'field',4,'de',' 38 '),
	(65,'slug',0,'de',' leonard '),
	(65,'title',0,'de',' leonard '),
	(66,'field',5,'de',' fin '),
	(66,'field',4,'de',' 13 '),
	(66,'slug',0,'de',' fin '),
	(66,'title',0,'de',' fin '),
	(67,'field',5,'de',' jan '),
	(67,'field',4,'de',' 7 '),
	(67,'slug',0,'de',' jan '),
	(67,'title',0,'de',' jan '),
	(68,'field',5,'de',' jan '),
	(68,'field',4,'de',' 44 '),
	(68,'slug',0,'de',' jan '),
	(68,'title',0,'de',' jan '),
	(69,'field',5,'de',' michael '),
	(69,'field',4,'de',' 12 '),
	(69,'slug',0,'de',' michael '),
	(69,'title',0,'de',' michael '),
	(70,'field',5,'de',' merle '),
	(70,'field',4,'de',' 34 '),
	(70,'slug',0,'de',' merle '),
	(70,'title',0,'de',' merle '),
	(71,'field',5,'de',' selma '),
	(71,'field',4,'de',' 13 '),
	(71,'slug',0,'de',' selma '),
	(71,'title',0,'de',' selma '),
	(72,'field',5,'de',' luana '),
	(72,'field',4,'de',' 13 '),
	(72,'slug',0,'de',' luana '),
	(72,'title',0,'de',' luana '),
	(73,'field',5,'de',' emmely '),
	(73,'field',4,'de',' 56 '),
	(73,'slug',0,'de',' emmely '),
	(73,'title',0,'de',' emmely '),
	(74,'field',5,'de',' dalleen '),
	(74,'field',4,'de',' 49 '),
	(74,'slug',0,'de',' dalleen '),
	(74,'title',0,'de',' dalleen '),
	(75,'field',5,'de',' daniel '),
	(75,'field',4,'de',' 49 '),
	(75,'slug',0,'de',' daniel '),
	(75,'title',0,'de',' daniel '),
	(76,'field',5,'de',' ben '),
	(76,'field',4,'de',' 32 '),
	(76,'slug',0,'de',' ben '),
	(76,'title',0,'de',' ben '),
	(77,'field',5,'de',' marcel '),
	(77,'field',4,'de',' 49 '),
	(77,'slug',0,'de',' marcel '),
	(77,'title',0,'de',' marcel '),
	(78,'field',5,'de',' jamel '),
	(78,'field',4,'de',' 18 '),
	(78,'slug',0,'de',' jamel '),
	(78,'title',0,'de',' jamel '),
	(79,'field',5,'de',' emrulah '),
	(79,'field',4,'de',' 40 '),
	(79,'slug',0,'de',' emrulah '),
	(79,'title',0,'de',' emrulah '),
	(80,'field',5,'de',' matthias '),
	(80,'field',4,'de',' 13 '),
	(80,'slug',0,'de',' matthias '),
	(80,'title',0,'de',' matthias '),
	(81,'field',5,'de',' david '),
	(81,'field',4,'de',' 11 '),
	(81,'slug',0,'de',' david '),
	(81,'title',0,'de',' david '),
	(82,'field',5,'de',' daniele '),
	(82,'field',4,'de',' 13 '),
	(82,'slug',0,'de',' daniele '),
	(82,'title',0,'de',' daniele '),
	(83,'field',5,'de',' landon '),
	(83,'field',4,'de',' 44 '),
	(83,'slug',0,'de',' landon '),
	(83,'title',0,'de',' landon '),
	(84,'field',5,'de',' michael '),
	(84,'field',4,'de',' 33 '),
	(84,'slug',0,'de',' michael '),
	(84,'title',0,'de',' michael '),
	(85,'field',5,'de',' jens '),
	(85,'field',4,'de',' 0 '),
	(85,'slug',0,'de',' jens '),
	(85,'title',0,'de',' jens '),
	(86,'field',5,'de',' marie '),
	(86,'field',4,'de',' 38 '),
	(86,'slug',0,'de',' marie '),
	(86,'title',0,'de',' marie '),
	(87,'field',5,'de',' thomas '),
	(87,'field',4,'de',' 69 '),
	(87,'slug',0,'de',' thomas '),
	(87,'title',0,'de',' thomas '),
	(88,'field',5,'de',' hendrik '),
	(88,'field',4,'de',' 52 '),
	(88,'slug',0,'de',' hendrik '),
	(88,'title',0,'de',' hendrik '),
	(89,'field',5,'de',' alfred '),
	(89,'field',4,'de',' 12 '),
	(89,'slug',0,'de',' alfred '),
	(89,'title',0,'de',' alfred '),
	(90,'field',5,'de',' michelle '),
	(90,'field',4,'de',' 36 '),
	(90,'slug',0,'de',' michelle '),
	(90,'title',0,'de',' michelle '),
	(91,'field',5,'de',' max '),
	(91,'field',4,'de',' 69 '),
	(91,'slug',0,'de',' max '),
	(91,'title',0,'de',' max '),
	(92,'field',5,'de',' rojin '),
	(92,'field',4,'de',' 23 '),
	(92,'slug',0,'de',' rojin '),
	(92,'title',0,'de',' rojin '),
	(93,'field',5,'de',' laurenz '),
	(93,'field',4,'de',' 41 '),
	(93,'slug',0,'de',' laurenz '),
	(93,'title',0,'de',' laurenz '),
	(94,'field',5,'de',' emrullah '),
	(94,'field',4,'de',' 40 '),
	(94,'slug',0,'de',' emrullah '),
	(94,'title',0,'de',' emrullah '),
	(95,'field',5,'de',' marie '),
	(95,'field',4,'de',' 42 '),
	(95,'slug',0,'de',' marie '),
	(95,'title',0,'de',' marie '),
	(96,'field',5,'de',' lena '),
	(96,'field',4,'de',' 54 '),
	(96,'slug',0,'de',' lena '),
	(96,'title',0,'de',' lena '),
	(97,'field',5,'de',' zeynab '),
	(97,'field',4,'de',' 49 '),
	(97,'slug',0,'de',' zeynab '),
	(97,'title',0,'de',' zeynab '),
	(98,'field',5,'de',' anton '),
	(98,'field',4,'de',' 20 '),
	(98,'slug',0,'de',' anton '),
	(98,'title',0,'de',' anton '),
	(99,'field',5,'de',' timo '),
	(99,'field',4,'de',' 15 '),
	(99,'slug',0,'de',' timo '),
	(99,'title',0,'de',' timo '),
	(100,'field',5,'de',' vinh '),
	(100,'field',4,'de',' 42 '),
	(100,'slug',0,'de',' vinh '),
	(100,'title',0,'de',' vinh '),
	(101,'field',5,'de',' joshua '),
	(101,'field',4,'de',' 57 '),
	(101,'slug',0,'de',' joshua '),
	(101,'title',0,'de',' joshua '),
	(102,'field',5,'de',' lukas '),
	(102,'field',4,'de',' 49 '),
	(102,'slug',0,'de',' lukas '),
	(102,'title',0,'de',' lukas '),
	(103,'field',5,'de',' lukas '),
	(103,'field',4,'de',' 49 '),
	(103,'slug',0,'de',' lukas '),
	(103,'title',0,'de',' lukas '),
	(104,'field',5,'de',' justin '),
	(104,'field',4,'de',' 60 '),
	(104,'slug',0,'de',' justin '),
	(104,'title',0,'de',' justin '),
	(105,'field',5,'de',' peter '),
	(105,'field',4,'de',' 36 '),
	(105,'slug',0,'de',' peter '),
	(105,'title',0,'de',' peter '),
	(106,'field',5,'de',' emily '),
	(106,'field',4,'de',' 56 '),
	(106,'slug',0,'de',' emily '),
	(106,'title',0,'de',' emily '),
	(107,'field',5,'de',' darleen '),
	(107,'field',4,'de',' 52 '),
	(107,'slug',0,'de',' darleen '),
	(107,'title',0,'de',' darleen '),
	(108,'field',5,'de',' alex '),
	(108,'field',4,'de',' 36 '),
	(108,'slug',0,'de',' alex '),
	(108,'title',0,'de',' alex '),
	(109,'field',5,'de',' tailan '),
	(109,'field',4,'de',' 11 '),
	(109,'slug',0,'de',' tailan '),
	(109,'title',0,'de',' tailan '),
	(110,'field',5,'de',' kevin '),
	(110,'field',4,'de',' 49 '),
	(110,'slug',0,'de',' kevin '),
	(110,'title',0,'de',' kevin '),
	(111,'field',5,'de',' kevin '),
	(111,'field',4,'de',' 36 '),
	(111,'slug',0,'de',' kevin '),
	(111,'title',0,'de',' kevin '),
	(112,'field',5,'de',' leonas '),
	(112,'field',4,'de',' 20 '),
	(112,'slug',0,'de',' leonas '),
	(112,'title',0,'de',' leonas '),
	(113,'field',5,'de',' jens '),
	(113,'field',4,'de',' 37 '),
	(113,'slug',0,'de',' jens '),
	(113,'title',0,'de',' jens '),
	(114,'field',5,'de',' michael '),
	(114,'field',4,'de',' 45 '),
	(114,'slug',0,'de',' michael '),
	(114,'title',0,'de',' michael '),
	(115,'field',5,'de',' marie '),
	(115,'field',4,'de',' 34 '),
	(115,'slug',0,'de',' marie '),
	(115,'title',0,'de',' marie '),
	(116,'field',5,'de',' oliver '),
	(116,'field',4,'de',' 34 '),
	(116,'slug',0,'de',' oliver '),
	(116,'title',0,'de',' oliver '),
	(117,'field',5,'de',' kai '),
	(117,'field',4,'de',' 18 '),
	(117,'slug',0,'de',' kai '),
	(117,'title',0,'de',' kai '),
	(118,'field',5,'de',' anna '),
	(118,'field',4,'de',' 12 '),
	(118,'slug',0,'de',' anna '),
	(118,'title',0,'de',' anna '),
	(119,'field',5,'de',' katharina '),
	(119,'field',4,'de',' 39 '),
	(119,'slug',0,'de',' katharina '),
	(119,'title',0,'de',' katharina '),
	(120,'field',5,'de',' leonas '),
	(120,'field',4,'de',' 49 '),
	(120,'slug',0,'de',' leonas '),
	(120,'title',0,'de',' leonas '),
	(121,'field',5,'de',' anette '),
	(121,'field',4,'de',' 43 '),
	(121,'slug',0,'de',' anette '),
	(121,'title',0,'de',' anette '),
	(122,'field',5,'de',' maik '),
	(122,'field',4,'de',' 38 '),
	(122,'slug',0,'de',' maik '),
	(122,'title',0,'de',' maik '),
	(123,'field',5,'de',' elsa '),
	(123,'field',4,'de',' 36 '),
	(123,'slug',0,'de',' elsa '),
	(123,'title',0,'de',' elsa '),
	(124,'field',5,'de',' peter '),
	(124,'field',4,'de',' 49 '),
	(124,'slug',0,'de',' peter '),
	(124,'title',0,'de',' peter '),
	(125,'field',5,'de',' teresa '),
	(125,'field',4,'de',' 6 '),
	(125,'slug',0,'de',' teresa '),
	(125,'title',0,'de',' teresa '),
	(126,'field',5,'de',' susanna '),
	(126,'field',4,'de',' 5 '),
	(126,'slug',0,'de',' susanna '),
	(126,'title',0,'de',' susanna '),
	(127,'field',5,'de',' david '),
	(127,'field',4,'de',' 39 '),
	(127,'slug',0,'de',' david '),
	(127,'title',0,'de',' david '),
	(128,'field',5,'de',' dustin '),
	(128,'field',4,'de',' 38 '),
	(128,'slug',0,'de',' dustin '),
	(128,'title',0,'de',' dustin '),
	(129,'field',5,'de',' leon '),
	(129,'field',4,'de',' 56 '),
	(129,'slug',0,'de',' leon '),
	(129,'title',0,'de',' leon '),
	(130,'field',5,'de',' christoph '),
	(130,'field',4,'de',' 53 '),
	(130,'slug',0,'de',' christoph '),
	(130,'title',0,'de',' christoph '),
	(131,'field',5,'de',' ralph '),
	(131,'field',4,'de',' 46 '),
	(131,'slug',0,'de',' ralph '),
	(131,'title',0,'de',' ralph ');

/*!40000 ALTER TABLE `craft_searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections`;

CREATE TABLE `craft_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_name_unq_idx` (`name`),
  UNIQUE KEY `craft_sections_handle_unq_idx` (`handle`),
  KEY `craft_sections_structureId_fk` (`structureId`),
  CONSTRAINT `craft_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections` WRITE;
/*!40000 ALTER TABLE `craft_sections` DISABLE KEYS */;

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'index',1,'2017-06-24 14:06:45','2017-06-24 14:06:45','105cc867-bc29-4ec9-b714-2cb645d73c0a'),
	(3,NULL,'Spieler','spieler','channel',0,NULL,1,'2017-06-24 14:27:25','2017-06-24 14:27:25','dad8ab7a-dc07-4c9f-a942-5a3e99d3a23c');

/*!40000 ALTER TABLE `craft_sections` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_sections_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections_i18n`;

CREATE TABLE `craft_sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `craft_sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections_i18n` WRITE;
/*!40000 ALTER TABLE `craft_sections_i18n` DISABLE KEYS */;

INSERT INTO `craft_sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'de',1,'__home__',NULL,'2017-06-24 14:06:45','2017-06-24 14:06:45','c5e5a856-c940-45b6-81e1-6fa88d90bb7b'),
	(3,3,'de',1,NULL,NULL,'2017-06-24 14:27:25','2017-06-24 14:27:25','cf635162-cb5a-4ea1-92cb-c28c4d2cd9c0');

/*!40000 ALTER TABLE `craft_sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sessions`;

CREATE TABLE `craft_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sessions_uid_idx` (`uid`),
  KEY `craft_sessions_token_idx` (`token`),
  KEY `craft_sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `craft_sessions_userId_fk` (`userId`),
  CONSTRAINT `craft_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sessions` WRITE;
/*!40000 ALTER TABLE `craft_sessions` DISABLE KEYS */;

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'e1db7102f50559ef1b9aa8ba371280351faad636czozMjoiX1lWOHBUTjV3RXhKTUk0Y1RNX0RGSFBjSUg1alZlNkQiOw==','2017-06-24 14:06:45','2017-06-24 14:06:45','b8980c1a-c739-4f8d-a1e7-4881263b888d'),
	(2,1,'a8ac68a9f5acce98cd1786d09f0bea31939235deczozMjoib19mZDRJR2dscVpyaDlHdVVzQUZfNHJVODJ4bUF+eGUiOw==','2017-06-24 15:34:26','2017-06-24 15:34:26','433345d8-d2b8-49a1-860b-b310710cafa5'),
	(3,1,'97e86b3263d1c02912d329fd89bdc30bf8bc3562czozMjoibEl+M0pEM1Q0SExTdmNVZkh3U2NQWHd4RndEVUtOUnQiOw==','2017-06-24 17:43:23','2017-06-24 17:43:23','d6164e90-1fdf-47c7-8b38-afd2d171dbb4'),
	(4,1,'2dc5dfff6f3bbf75f06133a19b9657cc7725f8d5czozMjoiYTByY0lRenFvd3FTdDFqbzIwRGphRHp5OWxocGRXZEsiOw==','2017-06-24 17:47:43','2017-06-24 17:47:43','90fe2b06-2d66-421d-a6ff-a275fd23c15c'),
	(6,1,'b6f092fdf173ce32a0cf6b56dff33a47a7ea1625czozMjoiN0JpeExzeXFVYWVIdzB4MF92dkRGbkQ2Nm9lRmpMdEMiOw==','2017-06-24 17:56:11','2017-06-24 18:29:31','6afd4178-7f2b-4ace-94dc-7624b9f8c7e0'),
	(7,1,'d78d7e82e4b7c1c7197879c3f2b58fa4d058fcc3czozMjoiRG9iT25Dbk5nVUFVZEpEWUhzU1FiVG1HaDN2SkRMS3MiOw==','2017-06-25 06:46:13','2017-06-25 06:46:13','43115af7-f9cd-4ad7-8f05-c1e188ce8f96'),
	(8,1,'18ffede5a1213115311b590d570a8d75108b6f67czozMjoiUnhwenE3SHNaYXpKZm5HfnNhazNBM2hSSTJvNms3RmEiOw==','2017-06-25 09:16:26','2017-06-25 10:23:48','04ec8258-81ba-45d5-8c9b-57c41b0a759e'),
	(9,1,'4bb65064e03bebbdb17711bfa5bc6f49cea1db75czozMjoiUFhPWldvNUFjNHJUd3RaX0huTGdOdG45aTVHZ2Jxa3MiOw==','2017-06-25 09:36:19','2017-06-25 15:02:47','dbeca74f-977a-441c-a385-b020aff56614'),
	(10,1,'efddc241e4d5335ba7b3e0bbf71ec1837337c8fdczozMjoiX3hJbHdzTHdiNTRhQ2c2NFZndDV1b1hBTG1DMkIySU0iOw==','2017-06-25 09:39:09','2017-06-25 09:39:09','5714870f-9e4d-4154-aa1b-c9152ca792e4'),
	(11,1,'987f6514ac660274fa94621a9c264b9d139cb883czozMjoieDl+aTViUVM2dFpPaHoyM1MxOXBXWDdIMTZQb3c3TXEiOw==','2017-06-25 12:56:07','2017-06-25 15:18:44','6270bf25-5516-4ae4-9a7f-6fc7450f9774');

/*!40000 ALTER TABLE `craft_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_shunnedmessages`;

CREATE TABLE `craft_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `craft_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structureelements`;

CREATE TABLE `craft_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `craft_structureelements_root_idx` (`root`),
  KEY `craft_structureelements_lft_idx` (`lft`),
  KEY `craft_structureelements_rgt_idx` (`rgt`),
  KEY `craft_structureelements_level_idx` (`level`),
  KEY `craft_structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `craft_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structures`;

CREATE TABLE `craft_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_systemsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_systemsettings`;

CREATE TABLE `craft_systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_systemsettings` WRITE;
/*!40000 ALTER TABLE `craft_systemsettings` DISABLE KEYS */;

INSERT INTO `craft_systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'email','{\"protocol\":\"php\",\"emailAddress\":\"andreas@kopfmunter.de\",\"senderName\":\"Bussimulator\"}','2017-06-24 14:06:45','2017-06-24 14:06:45','1d93b3cc-e0be-482f-bf3a-05cc050a4a50');

/*!40000 ALTER TABLE `craft_systemsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_taggroups`;

CREATE TABLE `craft_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_taggroups_handle_unq_idx` (`handle`),
  KEY `craft_taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_taggroups` WRITE;
/*!40000 ALTER TABLE `craft_taggroups` DISABLE KEYS */;

INSERT INTO `craft_taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default',1,'2017-06-24 14:06:45','2017-06-24 14:06:45','579ad2cd-a525-40df-884b-493c85474420');

/*!40000 ALTER TABLE `craft_taggroups` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tags`;

CREATE TABLE `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tags_groupId_fk` (`groupId`),
  CONSTRAINT `craft_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tasks`;

CREATE TABLE `craft_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` mediumtext COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tasks_root_idx` (`root`),
  KEY `craft_tasks_lft_idx` (`lft`),
  KEY `craft_tasks_rgt_idx` (`rgt`),
  KEY `craft_tasks_level_idx` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_tasks` WRITE;
/*!40000 ALTER TABLE `craft_tasks` DISABLE KEYS */;

INSERT INTO `craft_tasks` (`id`, `root`, `lft`, `rgt`, `level`, `currentStep`, `totalSteps`, `status`, `type`, `description`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(17,17,1,2,0,NULL,NULL,'pending','DeleteStaleTemplateCaches',NULL,'{\"elementId\":[\"21\",\"22\",\"23\",\"24\",\"25\",\"26\",\"27\",\"28\",\"29\",\"30\",\"31\",\"32\",\"33\",\"34\",\"35\",\"36\",\"37\",\"38\",\"39\",\"40\",\"41\",\"42\",\"43\",\"44\",\"45\",\"46\",\"47\",\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"54\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"68\",\"69\",\"70\",\"71\",\"72\",\"73\",\"74\",\"75\",\"76\",\"77\",\"78\",\"79\",\"80\",\"81\",\"82\",\"83\",\"84\",\"85\",\"86\",\"87\",\"88\",\"89\",\"90\",\"91\",\"92\",\"93\",\"94\",\"95\",\"96\",\"97\",\"98\",\"99\",\"100\",\"101\",\"102\",\"103\",\"104\",\"105\",\"106\",\"107\",\"108\",\"109\",\"110\",\"111\",\"112\",\"113\",\"114\",\"115\",\"116\",\"117\",\"118\",\"119\",\"120\",\"121\",\"122\",\"123\",\"124\",\"125\",\"126\",\"127\",\"128\",\"129\",\"130\",\"131\"]}','2017-06-25 09:41:34','2017-06-25 15:36:08','c3bb8eae-0662-4874-9b8e-99b34cead948');

/*!40000 ALTER TABLE `craft_tasks` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_templatecachecriteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecachecriteria`;

CREATE TABLE `craft_templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `craft_templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `craft_templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecacheelements`;

CREATE TABLE `craft_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `craft_templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `craft_templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `craft_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecaches`;

CREATE TABLE `craft_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `craft_templatecaches_locale_fk` (`locale`),
  CONSTRAINT `craft_templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tokens`;

CREATE TABLE `craft_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_tokens_token_unq_idx` (`token`),
  KEY `craft_tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups`;

CREATE TABLE `craft_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_usergroups_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups_users`;

CREATE TABLE `craft_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `craft_usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `craft_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions`;

CREATE TABLE `craft_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_usergroups`;

CREATE TABLE `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `craft_userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `craft_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_users`;

CREATE TABLE `craft_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `craft_userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `craft_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Export von Tabelle craft_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_users`;

CREATE TABLE `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_users_username_unq_idx` (`username`),
  UNIQUE KEY `craft_users_email_unq_idx` (`email`),
  KEY `craft_users_verificationCode_idx` (`verificationCode`),
  KEY `craft_users_uid_idx` (`uid`),
  KEY `craft_users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `craft_users_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_users` WRITE;
/*!40000 ALTER TABLE `craft_users` DISABLE KEYS */;

INSERT INTO `craft_users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'info@kvs.de',NULL,'','','info@kvs.de','$2y$13$FybRMkZAxvK2WESOUlWDaO2ENSRewQzJmz4j6iw3CEMOl4ANmtl9O',NULL,1,1,0,0,0,0,0,'2017-06-25 12:56:07','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2017-06-25 06:46:53','2017-06-24 14:06:41','2017-06-25 12:56:07','769aec6b-236a-4bdf-8df9-cbcaf8041820');

/*!40000 ALTER TABLE `craft_users` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle craft_widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_widgets`;

CREATE TABLE `craft_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_widgets_userId_fk` (`userId`),
  CONSTRAINT `craft_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_widgets` WRITE;
/*!40000 ALTER TABLE `craft_widgets` DISABLE KEYS */;

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'RecentEntries',1,NULL,NULL,1,'2017-06-24 14:06:50','2017-06-24 14:06:50','b64e5b58-2638-4c1e-916c-1fc0bf080adc'),
	(2,1,'GetHelp',2,NULL,NULL,1,'2017-06-24 14:06:50','2017-06-24 14:06:50','a4f6028c-d28a-4dbf-bcda-94be2aa34aa3'),
	(3,1,'Updates',3,NULL,NULL,1,'2017-06-24 14:06:50','2017-06-24 14:06:50','5ae2962e-d52a-4f2d-8120-3896dc4e8368'),
	(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',1,'2017-06-24 14:06:50','2017-06-24 14:06:50','fb4086ac-787f-408d-8428-f7763339f4ba');

/*!40000 ALTER TABLE `craft_widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
