var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var gutil = require('gulp-util');
var mkdirp = require('mkdirp');
var opn = require('opn');

var $ = require('gulp-load-plugins')();


// copy project specific files
gulp.task('init', function() {
	gulp.src('templates/db.php')
		.pipe(gulp.dest('craft/config'));

	gulp.src('templates/general.php')
		.pipe(gulp.dest('craft/config'));

	gulp.src('templates/_layout.html')
		.pipe(gulp.dest('craft/templates'));

	gulp.src('templates/index.html')
		.pipe(gulp.dest('craft/templates'));

    gulp.src('templates/header.html')
        .pipe(gulp.dest('craft/templates/partials'));

    gulp.src('templates/nav.html')
        .pipe(gulp.dest('craft/templates/partials'));

    gulp.src('templates/_entry.html')
        .pipe(gulp.dest('craft/templates/seiten'));

    gulp.src('templates/default.html')
        .pipe(gulp.dest('craft/templates/seiten/_types'));

	mkdirp('craft/storage', function (err) {
		if (err) console.error(err)
	});
	mkdirp('craft/plugins', function (err) {
		if (err) console.error(err)
	});
});

// Compile all SASS Files
gulp.task('styles', function () {
    return gulp.src('scss/app.scss')
        .pipe($.sass({ includePaths: ['bower_components/foundation-sites/scss'],errLogToConsole: true}))
        .pipe($.autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream())
        .pipe($.size())
        .pipe($.notify("Erfolgreich kompiliert."));;
});

// Lint JavaScripts, Uglify, Concat and rename
gulp.task('scripts', function () {
    return gulp.src(['public/js/plugins.js','public/js/app.js'])
        .pipe($.jshint())
        .pipe($.jshint.reporter(require('jshint-stylish')))
        .pipe($.size())
        .pipe($.concat('scripts.js'))
        .pipe(gulp.dest('public/js/concat'))
        .pipe($.rename('scripts.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('public/js/min'));

});

// Minify images
gulp.task('images', function () {
    return gulp.src('public/img/**/*')
        .pipe($.cache($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('public/img/min'))
        .pipe(reload({stream:true, once:true}))
        .pipe($.size());
});

// create a default task and just log a message
gulp.task('default', function() {
  console.log('DEFAULT');
});


gulp.task('serve', ['styles','scripts'], function () {
    browserSync.init(null, {
        proxy: 'bussimulator.craft.dev'
    }, function (err, bs) {
        // opn(bs.options.getIn(["urls", "local"]));
        console.log('Started connect web server on ' + bs.options.getIn(["urls", "local"]));
    });
});

gulp.task('watch', ['serve'], function () {
 
    // watch for changes
    gulp.watch(['craft/templates/**/*.html']).on('change', browserSync.reload);
 
    gulp.watch('scss/**/*.scss', ['styles']);
    gulp.watch(['public/js/plugins.js','public/js/app.js'], ['scripts']);
    gulp.watch('public/img/**/*', ['images']);
    // gulp.watch('bower.json', ['wiredep']);
});



