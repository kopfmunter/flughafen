var scoresApp = new Vue({

    el: '#app',
    delimiters: ['${', '}'],
    data: {
        players: [],
    }

});

setInterval(function(){
    axios.get('/api/scores.json').then(function(response){
        scoresApp.players = response.data.data;
    });
},4000);
    
