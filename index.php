<?php
switch ($_SERVER['SERVER_NAME']) {
    case "bussimulator.craft.dev":
        define('CRAFT_ENVIRONMENT', 'LOCAL');
        break;
    case "bussimulator.kopfmunter.de":
        define('CRAFT_ENVIRONMENT', 'STAGING');
        break;
    default:   // Covers "www" and non-"www"
        define('CRAFT_ENVIRONMENT', 'PRODUCTION');
        break;
}

if(CRAFT_ENVIRONMENT == 'STAGING') {

  $username = 'bussimulator';
    $password = 'km2bussimulator';

  if (!(isset($_SERVER['PHP_AUTH_USER']) && ($_SERVER['PHP_AUTH_USER']==$username && $_SERVER['PHP_AUTH_PW']==$password)))  {
        header('WWW-Authenticate: Basic realm="This site is protected"');
        header('HTTP/1.0 401 Unauthorized');
        // Fallback message when the user presses cancel / escape
        echo 'Access denied';
      exit;
    }
}

// Path to your craft/ folder
$craftPath = 'craft';

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (!is_file($path))
{
	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

require_once $path;
